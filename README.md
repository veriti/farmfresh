# Development Notes

## Branching

*Develop* branch will be the main branch for development; only released branches will be merged to master.

*Feature/[feature]* branch will contain any work in progress based on which module a developer is working on

## Particulars

__*Contact Systems Administrator for passwords__

#### FTP

```
# url
ftp.crankit.com.au
# user
farmfresh
```

#### cPanel

```
# url
http://farmfresh.crankit.com.au/cpanel
# user
farmfresh
```

#### Wordpress

*Administrator Account*

```
# user
crankit
```
