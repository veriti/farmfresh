<?php
// WordPress Environment
require("wp-config.php");
set_time_limit(0);
/*

// Delete all suppliers, product categories and products
mysql_query("DELETE FROM slx_terms WHERE term_id > 7");


// Relax to ensure everything is deleted before import starts
sleep(3);

$rows = $wpdb->get_results("SELECT * FROM tblsuppliers");

// Cycle through each supplier
foreach ($rows as $row) {

	// Insert supplier
	wp_insert_term($row->fldSupplierName,'suppliers');
	$parent_term = term_exists($row->fldSupplierName,'suppliers');
	$parent_term_id = $parent_term['term_id'];

	// Update supplier fields
	update_field('supplier_address',$row->fldSupplierAddress,'suppliers_'.$parent_term_id);
	update_field('supplier_phone',$row->fldSupplierPhone,'suppliers_'.$parent_term_id);
	update_field('supplier_notes',$row->fldSupplierNotes,'suppliers_'.$parent_term_id);
}

// Cycle through and import each parent and child category
$rows = $wpdb->get_results("SELECT * FROM tblproductcategories ORDER BY fldProductCategoryParentID ASC");

foreach ($rows as $row) {

	wp_insert_term($row->fldProductCategoryName,'product_cat');

	$exists = $wpdb->get_row("SELECT * FROM tblproductcategories WHERE fldProductCategoryID = '$row->fldProductCategoryParentID'");

	if($exists) {
		$parent_term = term_exists($exists->fldProductCategoryName,'product_cat');
		$parent_term_id = $parent_term['term_id'];

		wp_insert_term($row->fldProductCategoryName,'product_cat',array('parent'=>$parent_term_id));
	}
}
*/
mysql_query("DELETE FROM slx_posts WHERE post_type = 'product'");

// Cycle through the category to product relationship table and import all products to their correct categories.
$rows = $wpdb->get_results("SELECT * FROM tblproducts WHERE fldProductRemoved = '0' ORDER BY fldProductID");

foreach ($rows as $row) {

	// Select the product
	$product = $wpdb->get_row("SELECT * FROM tblproductcategorylinks WHERE fldProductID = '$row->fldProductID' LIMIT 1");

	// Select the category
	$category = $wpdb->get_row("SELECT * FROM tblproductcategories WHERE fldProductCategoryID = '$product->fldProductCategoryID' LIMIT 1");

	// Insert products
	$posts = array(
	    'post_title'  =>  $row->fldProductName,
	    'post_type' => 'product',
	    'post_status' => 'publish'
	);

	if($row->fldProductText) { 
		$posts['post_content'] = $row->fldProductText;
	}

	//print_r($posts);

	$post_id = wp_insert_post($posts, true); 
                              echo '<pre>';
                              print_r ($post_id);
                              echo '</pre>';

	// Simple Product
	wp_set_object_terms($post_id, 'simple', 'product_type');

	if($category) {
		wp_set_object_terms($post_id, $category->fldProductCategoryName, 'product_cat');
	}
	// Add SKU and Pricing
	add_post_meta($post_id, '_sku', $row->fldProductCode);
	add_post_meta($post_id, '_regular_price', $row->fldProductPriceEx);
	add_post_meta($post_id, '_price', $row->fldProductPriceEx);
	update_field('product_measure',$row->fldProductMeasure,$post_id);

	mysql_query("UPDATE slx_posts SET old_id = '$row->fldProductID' WHERE ID = '$post_id'");

	// Apply GST
	if($row->fldProductGSTExempt == 1) {
		add_post_meta($post_id, '_tax_status', 'none');
	} else { 
		add_post_meta($post_id, '_tax_status', 'taxable');
	}
 
 	// Set the supplier on the product if it exists
	$supplier = $wpdb->get_row("SELECT * FROM tblsuppliers WHERE fldSupplierID = '$row->fldSupplierID' LIMIT 1");

	if($supplier) {
		wp_set_object_terms($post_id, $supplier->fldSupplierName, 'suppliers');
	}
}
/*
// Cycle through each customer and import to wordpress users
$rows = $wpdb->get_results("SELECT * FROM tblcustomers");

foreach ($rows as $row) {

	$userdata = array(
	    'user_login'  =>  $row->fldCustomerUsername,
	    'user_pass'   =>  $row->fldCustomerPassword,
	    'first_name' => $row->fldCustomerFirstName,
	    'last_name' =>	$row->fldCustomerLastName,
	    'user_email' => $row->fldCustomerEmail,
	    'role' => 'customer',
	);

	$user_id = wp_insert_user($userdata);

	// Update customer profile & billing info
	update_user_meta($user_id, 'billing_first_name', $row->fldCustomerFirstName);
	update_user_meta($user_id, 'billing_last_name', $row->fldCustomerLastName);
	update_user_meta($user_id, 'billing_address_1', $row->fldCustomerAddress1);
	update_user_meta($user_id, 'billing_city', $row->fldCustomerCity);
	update_user_meta($user_id, 'billing_postcode', $row->fldCustomerPostcode);
	update_user_meta($user_id, 'billing_state', $row->fldCustomerState);
	update_user_meta($user_id, 'billing_country', 'AU');
	update_user_meta($user_id, 'billing_phone', $row->fldCustomerPhone);
	update_user_meta($user_id, 'billing_email', $row->fldCustomerEmail);

}
*/
?>