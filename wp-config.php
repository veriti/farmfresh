<?php

// BEGIN iThemes Security - Do not modify or remove this line
// iThemes Security Config Details: 2
define( 'DISALLOW_FILE_EDIT', true ); // Disable File Editor - Security > Settings > WordPress Tweaks > File Editor
// END iThemes Security - Do not modify or remove this line

	/**
	 * @author SponsoredLinX
	 * @package WordPress
	**/
	
	define('FS_METHOD', 'direct');

	define('WP_CACHE', TRUE);
	
	// Staging / Production Environment
	if(!in_array($_SERVER['REMOTE_ADDR'], array('127.0.0.1', '::1'))) {

		define('WP_HOME','http://farmfresh.crankit.com.au');
		define('WP_SITEURL','http://farmfresh.crankit.com.au');
			
		// ** MySQL settings - You can get this info from your web host ** //
		/** The name of the database for WordPress */
		define('DB_NAME', 'farmfres_wp');
		
		/** MySQL database username */
		define('DB_USER', 'farmfres_dbuser');
		
		/** MySQL database password */
		define('DB_PASSWORD', 'farmfresh#4755');
		
		/** MySQL hostname */
		define('DB_HOST', 'localhost');
		
		/** Database Charset to use in creating database tables. */
		define('DB_CHARSET', 'utf8');
		
		/** The Database Collate type. Don't change this if in doubt. */
		define('DB_COLLATE', '');
					
	}
	
	// Development Environment
	if(in_array($_SERVER['REMOTE_ADDR'], array('127.0.0.1', '::1'))) {
		
		define('WP_HOME','http://localhost/farmfresh/');
		define('WP_SITEURL','http://localhost/farmfresh/');
	
		// ** MySQL settings - You can get this info from your web host ** //
		/** The name of the database for WordPress */
		define('DB_NAME', 'farmfres_wp');
		
		/** MySQL database username */
		define('DB_USER', 'root');
		
		/** MySQL database password */
		define('DB_PASSWORD', '');
		
		/** MySQL hostname */
		define('DB_HOST', 'localhost');
		
		/** Database Charset to use in creating database tables. */
		define('DB_CHARSET', 'utf8');
		
		/** The Database Collate type. Don't change this if in doubt. */
		define('DB_COLLATE', '');	
	
		// Debugging
		// define('WP_DEBUG', true);
		
	}

	/**#@+
	 * Authentication Unique Keys and Salts.
	 *
	 * Change these to different unique phrases!
	 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
	 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
	 *
	 * @since 2.6.0
	 */
	define('AUTH_KEY',         'G~SU{]z|W-=[$,u~%)X>v6TPi,|MSFwx[9?Z#a?KXVFsPG+P7kue+]t!oX0zl|a|');
	define('SECURE_AUTH_KEY',  'pyvw4|fsH^[/OGUJCC9M&M,B2J[L`Jk/K4[,.VTvH0:a*Y#:/sSN(RVsQ6?q');
	define('LOGGED_IN_KEY',    'x[g*B<FiR yuweWd0i*]&%|j(*mH|&P!&:D>~Em-BXPzVeDdY?E>|Exi*y3a.pi9');
	define('NONCE_KEY',        'qQ#tjT.8o<NA7C]m8I;&EG0Zcm_i @ Y|}iqG[fB]`I6o}!<>!<-(~?lV|HV');
	define('AUTH_SALT',        'rz{S%-I-tk%+-8*]Qf3G%EdMxVEE^+2aJd&u[5!P5~6E`Dw,yMvDze_W]');
	define('SECURE_AUTH_SALT', 'VkWA}s9+`F=2NS>)Zu;tOY7`t&HTkZ6}UF9,<|UNM>cgbVnlR;27H[}8s~M]$#[J');
	define('LOGGED_IN_SALT',   '~Dk)9t!J|REd&Ns#4mqiJG&er7a6+n1n]OxSN@+v+o_:Y>+v?A#5fk68x^IbCG-G');
	define('NONCE_SALT',       '$1Si[tCFT R<&tbc7gEiArray-[;-)|{Pp>X,[p_g~i[Y6z;>Q9Ywl!dqkDfZv*');
	
	/**#@-*/
	
	/**
	 * WordPress Database Table prefix.
	 *
	 * You can have multiple installations in one database if you give each a unique
	 * prefix. Only numbers, letters, and underscores please!
	 */
	$table_prefix  = 'slx_';
	
	/**
	 * WordPress Localized Language, defaults to English.
	 *
	 * Change this to localize WordPress. A corresponding MO file for the chosen
	 * language must be installed to wp-content/languages. For example, install
	 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
	 * language support.
	 */
	define('WPLANG', '');
	
	/**
	 * For developers: WordPress debugging mode.
	 *
	 * Change this to true to enable the display of notices during development.
	 * It is strongly recommended that plugin and theme developers use WP_DEBUG
	 * in their development environments.
	 */
	
	define('WP_DEFAULT_THEME', 'base');
	
	/* That's all, stop editing! Happy blogging. */
	
	/** Absolute path to the WordPress directory. */
	if ( !defined('ABSPATH') )
		define('ABSPATH', dirname(__FILE__) . '/');
	
	/** Sets up WordPress vars and included files. */
	require_once(ABSPATH . 'wp-settings.php');
	
	?>