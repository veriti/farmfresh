<?php if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly ?>

<div id="extensonworks-updater" class="wrap">
	<?php screen_icon( 'index' ); ?><div class="wrap"><h2><?php echo $this->name; ?></h2>
<div id="col-container">
	<?php
echo '<div class="updated fade">' . wpautop( __( 'See below for a list of the ExtensionWorks products active on this installation.', 'ew-updater' ) ) . '</div>' . "\n";
?>
		<div class="col-wrap">
			<form id="activate-products" method="post" action="" class="validate">
				<input type="hidden" name="action" value="activate-products" />
				<input type="hidden" name="page" value="<?php echo esc_attr( $this->page_slug ); ?>" />
				<?php
require_once $this->classes_path . 'class-extensionworks-updater-licenses-table.php';
$this->list_table = new EW_Updater_Licenses_Table();
$this->list_table->data = $this->get_detected_products();
$this->list_table->prepare_items();
$this->list_table->display();
submit_button( __( 'Activate Products', 'ew-updater' ), 'button-primary' );
?>
			</form>
		</div><!--/.col-wrap-->
</div><!--/#col-container-->
</div><!--/#extensionworks-updater-->
