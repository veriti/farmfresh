<?php
/**
 * EW_Container
 *
 * EW_Container represent a shipping box that contains items
 *
 * @class       EW_Container
 * @version     1.0
 * @package     ExtensionWorks-Shipper/Classes
 * @author      Extension Works
 * @distributor   www.extensionworks.com
 */

class EW_Container {


    private $id              = '';

    private $outer_width     = 0.0;

    private $inner_width     = 0.0;

    private $outer_length    = 0.0;

    private $inner_length    = 0.0;

    private $outer_height    = 0.0;

    private $inner_height    = 0.0;

    private $net_weight          = 0.0;

    private $max_weight      = 0.0;

    private $box_weight      = 0.0;

    private $max_unit        = 0;

    private $girth           =0.0;

    private $is_enable       = false;

    private $is_letter       = false;

    private $label           = '';

    private $is_weight_based = false;

    private $dimension_unit;

    private $weight_unit;

    private $location;


    var $items = array();

    public function __construct( $box = array() ) {

        foreach ( $box as $key => $value ) {

            if ( isset( $this->$key ) )
                $this->$key = $value;
        }

    }


    public function get_city(){
        return $this->items[0]->get_city();
    }

    public function get_state(){
        return $this->items[0]->get_state();
    }

    public function get_location(){

        return $this->items[0]->get_location();
    }

    public function is_enabled() {

        return $this->is_enable;
    }

    public function is_letter_container() {
        return $this->is_letter;
    }

    public function is_weight_based() {
        return $this->is_weight_based;
    }


    /**
     * Get container label
     *
     * @return String
     */
    public function get_label() {
        return $this->label;
    }

    /**
     * Set container label
     *
     * @param String  $newlabel
     */
    public function set_label( $label ) {
        $this->label = $label;

        return $this;
    }


    /**
     * get container dimension unit
     *
     * @return string
     */
    public function get_dimension_unit() {
        return $this->dimension_unit;
    }

    /**
     * Set container dimension unit
     *
     * @param String  $dimension_unit
     */
    public function set_dimension_unit( $dimension_unit ) {
        $this->dimension_unit = $dimension_unit;

        return $this;
    }


    /**
     * Get container weight unit
     *
     * @return string
     */
    public function get_weight_unit() {
        return $this->weight_unit;
    }

    /**
     * Set container weight unit
     *
     * @param String  $newweight_unit
     */
    public function set_weight_unit( $weight_unit ) {

        $weight_unit = strtolower( $weight_unit );
        $units = array( 'kg', 'g' , 'lbs', 'oz' );

        if ( in_array( $weight_unit, $units ) )
            $this->weight_unit = $weight_unit;
        else
            throw new Exception( 'Cannot set weight unit '. $weight_unit . ' to container ' );

        return $this;
    }


    /**
     * Get max unit
     *
     * @return int
     */
    public function get_max_unit() {
        return $this->max_unit;
    }

    /**
     * set container max unit
     *
     * @param Int     $newmax_unit
     */
    public function set_max_unit( $max_unit ) {
        $this->max_unit = $max_unit;

        return $this;
    }



    /**
     * Get Container ID
     *
     * @return string
     */
    public function get_id() {
        return $this->id;
    }

    /**
     * Set container ID
     *
     * @param String  $newid
     */
    public function set_id( $id ) {
        $this->id = $id;

        return $this;
    }


    /**
     * Get container width
     *
     * @param string  $type 'inner' return inner width
     *                      'outer' return outer width
     *                      default to inner.
     * @return float
     */
    public function get_width( $type= 'inner' ) {

        switch ( $type ) {
        case 'outer':
            return isset( $this->outer_width ) ? (float)$this->outer_width : 0.0 ;
            break;

        default:
            return isset( $this->inner_width ) ? (float)$this->inner_width : 0.0 ;
            break;
        }


    }


    /**
     * Set container width
     *
     * @param float   $width
     *
     * @param string  $type  'inner' set inner width
     *                      'outer' set outer width
     *                      default to inner.
     * @return float
     */
    public function set_width( $width, $type= 'inner' ) {

        switch ( $type ) {
        case 'outer':
            $this->outer_width = $width;
            break;

        default:
            $this->inner_width = $width;
            break;
        }

        return $this;

    }


    /**
     * Get container height
     *
     * @param string  $type 'inner' return inner height
     *                      'outer' return outer height
     *                      default to inner.
     * @return float
     */
    public function get_height( $type = 'inner' ) {

        switch ( $type ) {
        case 'outer':
            return isset( $this->outer_height ) ? (float)$this->outer_height : 0.0 ;
            break;

        default:
            return isset( $this->inner_height ) ? (float)$this->inner_height : 0.0 ;
            break;
        }


    }

    /**
     * Set container height
     *
     * @param float   $height
     *
     * @param string  $type   'inner' set inner height
     *                      'outer' set outer height
     *                      default to inner.
     * @return float
     */
    public function set_height( $height, $type= 'inner' ) {

        switch ( $type ) {
        case 'outer':
            $this->outer_height = $height;
            break;

        default:
            $this->inner_height = $height;
            break;
        }

        return $this;

    }

    /**
     * Get container length
     *
     * @param string  $type 'inner' return inner length
     *                      'outer' return outer length
     *                      default to inner.
     * @return float
     */
    public function get_length( $type ='inner' ) {

        switch ( $type ) {
        case 'outer':
            return isset( $this->outer_length ) ? (float)$this->outer_length : 0.0 ;
            break;

        default:
            return isset( $this->inner_length ) ? (float)$this->inner_length : 0.0 ;
            break;
        }

    }

    /**
     * Set container length
     *
     * @param float   $length
     *
     * @param string  $type   'inner' set inner length
     *                      'outer' set outer length
     *                      default to inner.
     * @return float
     */
    public function set_length( $length, $type= 'inner' ) {

        switch ( $type ) {
        case 'outer':
            $this->outer_length = $length;
            break;

        default:
            $this->inner_length = $length;
            break;
        }

        return $this;

    }


    /**
     * Set container outer dimensions
     *
     * @param float   $length
     * @param float   $width
     * @param float   $height
     */
    public function set_outer_dimension( $length, $width, $height ) {

        $this->set_length( $length, 'outer' );

        $this->set_width( $width, 'outer' );

        $this->set_height( $height, 'outer' );

        return $this;
    }


    /**
     * Set container inner dimensions
     *
     * @param float   $length
     * @param float   $width
     * @param float   $height
     */
    public function set_inner_dimension( $length, $width, $height ) {

        $this->set_length( $length );

        $this->set_width( $width );

        $this->set_height( $height );


        return $this;
    }



    /**
     * put an product in to container
     *
     * @param EW_Product $item
     * @return
     */
    public function put( $item ) {

        if ( $item instanceof EW_Product )
            $this->items[] =clone $item;
        else
            throw new Exception( "The Item is not our product" );

        return $this;
    }




    public function get_quantity() {
        return count( $this->items );
    }




    /**
     * Get Container's volume
     * Default: box. return container's volume
     * type = available return remaining volume
     * type = used return used volume
     *
     * @return float
     */
    public function get_volume( $type ='box' ) {

        switch ( $type ) {
        case 'available':
            return $this->get_available_volume();
        case 'used':
            return $this->get_used_volume();
        default:
            return $this->get_width() * $this->get_length() * $this->get_height();
        }

    }


    /**
     * Calculate container's used volume
     *
     * @return float
     */
    private function get_used_volume() {
        $volume = 0.0;
        foreach ( $this->items as $item ) {
            $volume += $item->get_volume();
        }

        return $volume;
    }

    /**
     * Get available volume in container
     *
     * @return [type] [description]
     */
    private function get_available_volume() {
        return $this->get_volume() - $this->get_used_volume();
    }

    public function set_weight( $weight , $type='box' ) {
        switch ( $type ) {
        case 'net':
            $this->net_weight  = $weight;
        case 'max':
            $this->max_weight = $weight;
            break;
        default:
            $this->box_weight = $weight;
        }

        return $this;
    }

    /**
     * Get container's various weight type
     *
     * @param string  $type 'gross' return container's gross weight
     *                      'net' return container's net weight
     *                      'box' return container's weight
     *                      'available' return remaining weight
     *                      'max' return container's max weight
     *                      Default to gross
     * @return float
     */
    public function get_weight( $type = 'gross' ) {

        switch ( $type ) {
        case 'net':
            return $this->get_net_weight();
            break;
        case 'box':
            return $this->box_weight;
            break;
        case 'available':
            return $this->get_available_weight();
            break;
        case 'max':
            return $this->get_max_weight();
            break;
        default:
            return $this->get_gross_weight();
            break;
        }
    }

    private function get_net_weight() {

        if ( count( $this->items ) === 0 )
            return $this->net_weight;


        $weight = 0.0;
        foreach ( $this->items as $item ) {
            $weight += $item->get_weight();
        }
        $this->net_weight = $weight;
        return $weight;
    }

    private function get_gross_weight() {
        return $this->get_net_weight() + $this->box_weight;
    }

    private function get_available_weight() {

        return $this->get_max_weight() - $this->get_net_weight();
    }

    /**
     * get container max weight
     *
     * @return float
     */
    private function get_max_weight() {
        return isset( $this->max_weight ) ? (float)$this->max_weight : 0.0 ;
    }


    /**
     * get container girth
     *
     * @return float
     */
    public function get_girth( $type = 'inner' ) {


        $dimension = array();

        switch ( $type ) {
        case 'inner':
            $dimension = array( $this->inner_length, $this->inner_width, $this->inner_height );
            break;

        default:
            $dimension = array( $this->outer_length, $this->outer_width, $this->outer_height );
            break;
        }

        sort( $dimension );

        return round( ( $dimension[0]* 2 ) + ( $dimension[1] * 2 ) );

    }

    /**
     * get price for this container, inclued price of items
     *
     * @return float
     */
    public function get_price() {

        $price = 0.0;
        foreach ( $this->items as $item ) {
            $price += $item->get_price();
        }
        return $price;
    }

    public function get_items() {
        return $this->items;
    }


    public function to_array(){

        $data = array(
            'id' => $this->get_id(),
            'outer_width' => $this->get_width('outer'),
            'inner_width' => $this->get_width('inner'),
            'outer_length' => $this->get_length('outer'),
            'inner_length' => $this->get_length('inner'),
            'outer_height' => $this->get_height('outer'),
            'inner_height' => $this->get_height('inner'),
            'net_weight' => $this->get_weight('net'),
            'max_weight' => $this->get_weight('max'),
            'box_weight' => $this->get_weight('box'),
            'gross_weight' => $this->get_weight('gross'),
            'max_unit' => $this->get_max_unit(),
            'girth' => $this->get_girth('outer'),
            'is_enable' => $this->is_enabled(),
            'is_letter' => $this->is_letter_container(),
            'label' => $this->get_label(),
            'is_weight_based' => $this->is_weight_based(),
            'dimension_unit' => $this->get_dimension_unit(),
            'weight_unit' => $this->get_weight_unit(),
        );

        foreach ($this->items as $key => $item) {
            $data['items'][ $key ] = $item->to_array();
        }


        return $data;
    }


}

?>
