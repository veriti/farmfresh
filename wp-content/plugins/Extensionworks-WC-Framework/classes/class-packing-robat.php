<?php

class EW_Packing_Robat {

    /**
     *
     *
     * @var array
     */
    var $items = array();

    /**
     *
     *
     * @var array
     */
    var $containers = array();

    /**
     *
     *
     * @var array
     */
    var $unpackable_items = array();

    /**
     *
     *
     * @var array
     */
    var $packable_items = array();
    /**
     *
     *
     * @var array
     */
    var $weight_limit = 0;

    /**
     *
     *
     * @var string
     */
    var $weight_unit = 'lbs';

    /**
     *
     *
     * @var string
     */
    var $dimension_unit = 'in';

    /**
     *
     *
     * @var string
     */
    var $from_weight_unit = 'lbs';

    /**
     *
     *
     * @var string
     */
    var $from_dimension_unit = 'in';

    /**
     *
     *
     * @var boolean
     */
    var $weight_only = false;

    /**
     *
     *
     * @var [type]
     */
    var $packing_method;

    /**
     *
     *
     * @var string
     */
    var $packing_type = 'togather';

    var $debug = false;

    var $letter_setting = 'together';


    public function __construct( $dimension_unit='in', $weight_unit='lbs' ) {

        $this->set_weight_unit( $weight_unit );
        $this->set_dimension_unit( $dimension_unit );

    }

    public function pick_item( $item ) {

        if ( $item instanceof EW_Product )
            throw new Exception( "Robot pick up an unknown type item" );

        $item = $this->reset_dimension( $item );

        $this->items[] = clone $item;

    }

    public function pick_container( $container ) {

        if ( $container instanceof EW_Container )
            throw new Exception( "Robot pick up an unknown type container" );

        $container = $this->reset_dimension( $container );

        $this->containers[] = clone $container;

    }

    public function reset_dimension( $container ) {
        $container = $this->reset_position( $container );

        $container = $this->unify_dimensions( $container );
        $container = $this->unify_weight( $container );

        return $container;
    }



    public function set_packing_type( $type ) {

        $this->packing_type = $type;
    }


    public function prepare_packing() {

        $items = array();

        if( count( $this->items ) > 0 ){

            $this->add_debug_message( "From " . $this->items[0]->get_dimension_unit() . ' to ' . $this->dimension_unit , 'Packing robot convert dimension');

            $this->add_debug_message( "From " . $this->items[0]->get_weight_unit() . ' to ' . $this->weight_unit , 'Packing robot convert weight');
        }

        foreach ( $this->items as $item ) {
            $items[] = $this->reset_dimension( $item );
        }

        $this->items = $this->volume_sort( $items );

        $this->add_debug_message( $this->items , 'Packing robot reset and sort items');

        $containers = array();
        foreach ( $this->containers as $container ) {
            $containers[] = $this->reset_dimension( $container );
        }

        $this->containers = $this->volume_sort( $containers );

        $this->packing_method->set_debug( $this->debug );

        $this->packing_method->set_letter_mail_setting( $this->letter_setting );

        try {

            //packing method pick up containers
            $this->packing_method->pick_containers( $this->containers );

            //packing method pick itmes
            $this->packing_method->pick_items( $this->items );

        } catch (Exception $e) {
            $this->add_debug_message( $e->getMessage() , 'Packing Robot Error');
        }  

    }

    public function set_letter_mail_setting( $letter_setting ) {

        $this->letter_setting = $letter_setting;

    }


    public function packing() {

        $this->prepare_packing();

        $this->packing_method->packing();

        return array( 'packages'=> $this->packing_method->get_packages(), 'unpacked' => $this->packing_method->get_unpackable_items() );
    }

    public function set_packing_method( $method ) {
        $this->packing_method = $method;
    }

    public function set_items( $items ) {
        $this->items = $items;
    }

    public function set_containers( $containers ) {
        $this->containers = $containers;
    }

    public function get_items() {
        return $this->items;
    }

    public function get_packages() {
        return $this->packing_method->get_packages();
    }

    public function get_unpackable_products() {
        return $this->packing_method->get_unpackable_products();
    }

    function convert_dimension( $dim, $from_unit, $to_unit ) {

        $from_unit  = strtolower( $from_unit );
        $to_unit = strtolower( $to_unit );

        // Unify all units to cm first
        if ( $from_unit !== $to_unit ) {

            switch ( $from_unit ) {
            case 'in':
                $dim *= 2.54;
                break;
            case 'm':
                $dim *= 100;
                break;
            case 'mm':
                $dim *= 0.1;
                break;
            case 'yd':
                $dim *= 0.010936133;
                break;
            }

            // Output desired unit
            switch ( $to_unit ) {
            case 'in':
                $dim *= 0.3937;
                break;
            case 'm':
                $dim *= 0.01;
                break;
            case 'mm':
                $dim *= 10;
                break;
            case 'yd':
                $dim *= 91.44;
                break;
            }
        }
        return ( $dim < 0 ) ? 0 : $dim;
    }

    function convert_weight( $weight, $from_unit, $to_unit ) {

        $from_unit  = strtolower( $from_unit );
        $to_unit = strtolower( $to_unit );

        //Unify all units to kg first
        if ( $from_unit !== $to_unit ) {

            switch ( $from_unit ) {
            case 'g':
                $weight *= 0.001;
                break;
            case 'lbs':
                $weight *= 0.4536;
                break;
            case 'oz':
                $weight *= 0.0283;
                break;
            }

            // Output desired unit
            switch ( $to_unit ) {
            case 'g':
                $weight *= 1000;
                break;
            case 'lbs':
                $weight *= 2.2046;
                break;
            case 'oz':
                $weight *= 35.274;
                break;
            }
        }
        return ( $weight < 0 ) ? 0 : $weight;
    }

    public function set_dimension_unit( $unit ) {

        if ( $unit )
            $this->dimension_unit = $unit;
    }

    public function set_weight_unit( $unit ) {
        if ( $unit )
            $this->weight_unit = $unit;
    }

    /**
     * Reset the entity to have its dimension in such order:
     * Height <= Width <= Length
     */
    public function reset_position( $entity ) {

        $dimensions = array( $entity->get_height(),
            $entity->get_width(),
            $entity->get_length()
        );

        sort( $dimensions );

        // height < width < length
        $entity->set_height( $dimensions[0] );
        $entity->set_width( $dimensions[1] );
        $entity->set_length( $dimensions[2] );

        return $entity;
    }

    /**
     * convert weight to something usable by the package
     */
    public function unify_weight( $item ) {

        $item->set_weight( $this->convert_weight( $item->get_weight(), $item->get_weight_unit(), $this->weight_unit ) );
        $item->set_weight_unit( $this->weight_unit );

        return $item;
    }

    /**
     * convert dimensions to something usable by the package
     */
    public function unify_dimensions( $item ) {

        $dimensions = array( 'width', 'height', 'length' );
        foreach ( $dimensions as $dimension ) {

            $item->{'set_'. $dimension}( $this->convert_dimension( $item->{'get_'.$dimension}(), $item->get_dimension_unit(), $this->dimension_unit ) );

        }
        $item->set_dimension_unit( $this->dimension_unit );

        return $item;

    }


    /**
     * order true is descent,
     *
     * @param [type]  $products [description]
     * @param boolean $order    [description]
     * @return [type]            [description]
     */
    function volume_sort( $items, $order = 'asc' ) {

        if ( is_array( $items ) )
            usort( $items, array( $this, 'compare_volume' ) );

        if ( $order == 'desc' )
            return array_reverse( $items );

        return $items;
    }



    public  function compare_volume( $alpha, $omega ) {

        if ( $alpha->get_volume() == $omega->get_volume() ) {
            return 0;
        }
        return ( $alpha->get_volume() > $omega->get_volume() ) ? 1 : -1;
    }


    /**
     * Turn debug on
     */
    public function set_debug_on() {
        $this->debug = true;
    }


    /**
     * Turn debug off
     */
    public function set_debug_off() {
        $this->debug = false;
    }



    /**
     * Get all containers
     *
     * @return array
     */
    public function get_containers() {
        return $this->containers;
    }


    /**
     * Add debug message
     *
     * @param mix     $msg
     * @param String  $title
     */
    public function add_debug_message( $msg, $title ) {
        if ( $this->debug != 'yes' )
            return;

        ew_add_response_message( $msg, $title );
    }

}

?>
