<?php

class EW_Box_Packing extends EW_Packing_Algorithm{


	public function packing_letter_together() {

		if( !empty( $this->items['box'] ) ){

			//remove letter containers
			$this->containers = $this->containers['box'];

			$this->items = array_merge( $this->items['letter'], $this->items['box'] );
		}else{

			$this->containers = array_merge( $this->containers['letter'] );
			$this->items = array_merge( $this->items['letter'], $this->items['box'] );
		}

		usort( $this->items, array( $this, 'compare_volume' ) );

		usort( $this->containers, array( $this, 'compare_volume' ) );

		$package['unpacked'] = $this->items;

		do {
			$package = $this->find_best_package( $this->containers, $package['unpacked'] );

			if ( $package['percent'] !== 0 )
				$this->packages[] = $package;

		}while ( $package['percent'] !== 100 && $package['percent'] !== 0 );

		$this->unpackable_items = $package['unpacked'];
	}

	public function packing_letter_separate() {

		//letter
		if ( isset( $this->items['letter'] ) ) {

			$package['unpacked'] = $this->items['letter'];



			if ( !isset( $this->containers['letter'] ) )
				$this->containers['letter'] = array();

			do {

				$package = $this->find_best_package( $this->containers['letter'], $package['unpacked'] );

				if( $package['percent'] != 0 )
					$this->packages['letter'][] = $package;

			}while ( $package['percent'] !== 100 && $package['percent'] !== 0 );


			$this->unpackable_items['letter'] = $package['unpacked'];
		}



		//box
		if ( isset( $this->items['box'] ) ) {

			$package['unpacked'] = $this->items['box'];

			if ( !isset( $this->containers['box'] ) )
				$this->containers['box'] = array();
			do {

				$package = $this->find_best_package( $this->containers['box'], $package['unpacked'] );

				if( $package['percent'] != 0 )
					$this->packages['box'][] = $package;

			}while ( $package['percent'] !== 100 && $package['percent'] !== 0 );

			$this->unpackable_items['box'] = $package['unpacked'];

		}
	}


	/**
	 * find best percent container
	 *
	 * @return [type] [description]
	 */
	function find_best_package( $containers, $items ) {


		$best_package = array( 'percent' => 0, 'unpacked' => $items );
		foreach ( $containers as $key => $container ) {

			$package = $this->packing_items( clone $container, $items );

			//find best package
			if ( $package['percent'] > $best_package['percent'] )
				$best_package = $package;
		}

		if( isset( $best_package['container'] ) ){
			$msg = "Find best container( " . $best_package['container']->get_label()." ) for items ";

			foreach ( $best_package['container']->get_items() as $key => $item) {
				$msg .= $item->get_id() .', ';
			}
			$this->add_debug_message( $msg , 'Box packing');
		}else{

			$msg = 'Cannot find suitable container for items ';

			foreach ( $items as $key => $item) {
				$msg .= $item->get_id() . ', ';
			}

			$this->add_debug_message( $msg , 'Box packing');
		}
		
		return $best_package;

	}

	/**
	 * packing all items to conainer and calculate percent
	 *
	 * @param [type]  $container [description]
	 * @param [type]  $items     [description]
	 * @return [type]            [description]
	 */
	protected function packing_items( $container, $items ) {

		//keep original containers clean

		$container = clone $container;
		$unpackable_items = array();

		$packed_items = array();

		$percent = 0;

		$remaining_volume = $container->get_volume( 'available' );
		
		if( count( $items ) > 0 ){

			while ( count( $items ) >0 ) {
				$item = array_shift( $items );

				//check container dimensioins
				if ( !$this->can_fit( $container, $item ) )
					$unpackable_items[] = $item;
				else
					$container->put( $item );
			}

			$percent = ( sizeof( $container->get_items() ) / (  sizeof( $unpackable_items ) + sizeof( $container->get_items() ) ) ) * 100;

			$remaining_volume = $container->get_volume( 'available' );
		}

		return array( 'container' => $container,
			'percent'          => $percent,
			'unpacked'         => $unpackable_items,
			'remaining_volume' => $remaining_volume );
	}


	/**
	 * Check the item can fit in container or not
	 *
	 * @param EW_Container $container
	 * @param EW_Product $item
	 * @return boolen
	 */
	protected function can_fit( $container, $item ) {

		//check dimensions
		if ( $item->get_length() > $container->get_length() ||
			$item->get_width() > $container->get_width() ||
			$item->get_height() > $container->get_height()
		) {
			return false;
		}

		//check max weight
		if (  $container->get_weight('max') != 0 && $item->get_weight() > $container->get_weight( 'available' ) ) {
			// echo "max weight false\n";
			return false;
		}

		//check volume
		if ( $item->get_volume() > $container->get_volume( 'available' ) ) {
			// echo "max weight false\n";
			return false;
		}

		//check max dimension
		// if ( !$this->check_max_dimension( $container, $item ) ) {
		// 	// echo "max weight false\n";
		// 	return false;
		// }

		return true;
	}

	/** sum of itmes' height(lowest) must small than container highest dimension(length) **/
	public function check_max_dimension( $container, $item ) {

		$items_height = $item->get_height();

		foreach ( $container->get_items() as $key => $item ) {
			$items_height += $item->get_height();
		}

		if ( $items_height > $container->get_length() )
			return false;
		else
			return true;
	}

    public  function compare_volume( $alpha, $omega ) {

        if ( $alpha->get_volume() == $omega->get_volume() ) {
            return 0;
        }
        return ( $alpha->get_volume() > $omega->get_volume() ) ? 1 : -1;
    }

}

?>
