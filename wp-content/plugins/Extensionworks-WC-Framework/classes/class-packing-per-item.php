<?php
include_once 'class-packing-box-packing.php';
class EW_Per_Item_Packing extends EW_Box_Packing{


	public function packing_letter_separate() {

		if ( isset( $this->items['letter'] ) ) {

			$packages['unpacked'] = $this->items['letter'];

			if ( !isset( $this->containers['letter'] ) )
				$this->containers['letter'] = array();

			foreach ( $packages['unpacked'] as $key => $item ) {

				foreach ( $this->containers['letter'] as  $container ) {
					if ( $this->can_fit( $container, $item ) ) {
						$new_container = clone $container;
						$new_container->put( $item );
						unset( $packages['unpacked'][$key] );
						$this->packages['letter'][] = array( 'container' =>$new_container );
						break;
					}

				}
			}

			$this->unpackable_items['letter'] = $packages['unpacked'];
		}

		if ( isset( $this->items['box'] ) ) {

			$packages['unpacked'] = $this->items['box'];
			if ( !isset( $this->containers['box'] ) )
				$this->containers['box'] = array();

			foreach ( $packages['unpacked'] as $key => $item ) {

				foreach ( $this->containers['box'] as  $container ) {
					if ( $this->can_fit( $container, $item ) ) {

						$new_container = clone $container;
						$new_container->put( $item );
						unset( $packages['unpacked'][$key] );
						$this->packages['box'][] = array( 'container' =>$new_container );
						break;
					}

				}
			}

			$this->unpackable_items['box'] = $packages['unpacked'];
		}

	}


	public function packing_letter_together() {

		if( !empty( $this->items['box'] ) ){

			//remove letter containers
			$this->containers = $this->containers['box'];

			$this->items = $this->items['letter'] + $this->items['box'];
		}else{
			$this->containers = $this->containers['box'] + $this->containers['letter'];

			$this->items = $this->items['letter'] + $this->items['box'];
		}

		usort( $this->items, array( $this, 'compare_volume' ) );

		usort( $this->containers, array( $this, 'compare_volume' ) );

		$packages['unpacked'] = $this->items;


		foreach ( $packages['unpacked'] as $key => $item ) {

			foreach ( $this->containers as  $container ) {
				if ( $this->can_fit( $container, $item ) ) {
					$new_container = clone $container;
					$new_container->put( $item );
					unset( $packages['unpacked'][$key] );
					$this->packages[] = array( 'container'=>$new_container );
					break;
				}

			}
		}

		$this->unpackable_items = $packages['unpacked'];
	}

	protected function can_fit( $container, $item ) {

		if ( $item->get_length() > $container->get_length() ||
			$item->get_width() > $container->get_width() ||
			$item->get_height() > $container->get_height()
		) {

			return false;
		}

		//check max weight
		if ( ( $item->get_weight() > $container->get_weight( 'max' ) ) && $container->get_weight( 'max' ) > 0 ) {
			return false;
		}

		//check available weight
		if ( $item->get_volume() > $container->get_volume() ) {
			return false;
		}

		return true;
	}

}

?>
