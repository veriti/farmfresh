<?php

if( !function_exists( 'ew_clean' ) ){
	function ew_clean( $var ){
		return sanitize_text_field( $var );
	}
}

if ( !function_exists( 'ew_add_response_message' ) ) {
	function ew_add_response_message( $msg, $title, $type = 'message' ) {

		global $woocommerce;

		$message = '';
		if ( !is_string( $msg ) )
			$message = print_r( $msg, true );
		else
			$message = $msg;

		$debug_message = "<pre><strong>" . $title . "</strong>\n" . $message . "</pre>";

		if ( $type == 'message' ){

			if( check_woo_version( '2.1' ) )
				if( function_exists('wc_add_notice') )
					wc_add_notice( $debug_message, 'notice' );
			else
				$woocommerce->add_message( $debug_message );

		}else{
			if( check_woo_version( '2.1' ) )
				if( function_exists('wc_add_notice') )
					wc_add_notice( $debug_message, 'error' );
			else
				$woocommerce->add_error( $debug_message );

		}
	}
}

if ( !function_exists( 'ew_notification' ) ) {
	function ew_notification() {

		do_action( 'extensionworks_notification' );
	}
}
add_action( 'admin_notices', 'ew_notification' );


if ( !function_exists( 'ew_cart_shipping_packages' ) ) {
	function ew_cart_shipping_packages( $packages ){

	    $item_p = array();

	    $newpackage = array();

	    foreach ( WC()->cart->get_cart() as $item ) {

	        if( $item['data']->needs_shipping() ){
	            $option_data = get_post_custom( $item['product_id'] );

	            if( isset( $option_data['_ship'] ) ){
	                $item_p[$option_data['_ship'][0] ][] = $item;

	            }else{
	                $item_p['0' ][] = $item;
	            }

	        }
	    }

	    foreach($item_p as $key => $vendor_items) {

	        $package = array(
	            'contents' => $vendor_items,
	            'contents_cost' => array_sum( wp_list_pluck( $vendor_items, 'line_total' ) ),
	            'applied_coupons' => WC()->cart->applied_coupons,
	            'destination' => array(
	                'country' => WC()->customer->get_shipping_country(),
	                'state' => WC()->customer->get_shipping_state(),
	                'postcode' => WC()->customer->get_shipping_postcode(),
	                'city' => WC()->customer->get_shipping_city(),
	                'address' => WC()->customer->get_shipping_address(),
	                'address_2' => WC()->customer->get_shipping_address_2()
	            )
	        );

	        if( $key != '0'){

	            $package['ship_via'] = array( $key );
	        }
	        $newpackage[] = $package;
	    }	    

	    return $newpackage;
	}
}


?>
