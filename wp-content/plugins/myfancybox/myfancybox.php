<?php     
   /*
    Plugin Name: myFancybox 
    Plugin URI: http://wordpress.transformnews.com/plugins/myfancybox-simple-fancybox-implementation-for-wordpress-4
    Description: Simple jQuery Implementation of the FancyBox v2.1.5 for Wordpress. 
	fancyBox is created by Janis Skarnelis and original source code can be found at http://fancyapps.com/fancybox/ 
	fancyBox is licensed under Creative Commons Attribution-NonCommercial 3.0 license - http://creativecommons.org/licenses/by-nc/3.0/ 
    Version: 1.0 beta
    Author: m.r.d.a
    License: Creative Commons 
    */
 
//  create Fancybox Function  //
function fancybox() {
?>
 
<script type="text/javascript">
 
jQuery(document).ready(function($){	
		var select = $('a[href$=".bmp"],a[href$=".gif"],a[href$=".jpg"],a[href$=".jpeg"],a[href$=".png"],a[href$=".BMP"],a[href$=".GIF"],a[href$=".JPG"],a[href$=".JPEG"],a[href$=".PNG"]'); // select image files
		select.attr('data-fancybox-group', 'gallery'); //data-fancybox-group is used instead of rel tag in html5
		select.attr('class', 'fancybox'); // ads class tag to image
		select.fancybox();	
 
$(".fancybox").fancybox({
	 beforeShow: function () {
         this.title = $(this.element).find("img").attr("alt"); // shows alt tag as image title
    },
		padding:0, //  settings can be removed to use defaults  //
		margin  : 20,
		wrapCSS    : '',
		openEffect : 'elastic',
		openSpeed  : 650,
                arrows    : true,
	        closeEffect : 'elastic',
                closeSpeed  : 650,
                closeBtn  : true,
	        closeClick : false,
                nextClick  : false,
		prevEffect : 'elastic',
  		prevSpeed  : 650,
		nextEffect : 'elastic',
		nextSpeed  : 650,
		pixelRatio: 1, // Set to 2 for retina display support
	        autoSize   : true,
	        autoHeight : false,
	        autoWidth  : false,
	        autoResize  : true,
	        fitToView   : true,
		aspectRatio : false,
		topRatio    : 0.5,
		leftRatio   : 0.5,
		scrolling : 'auto', // 'auto', 'yes' or 'no'
	        mouseWheel : true,
		autoPlay   : false,
	        playSpeed  : 3000,
		preload    : 3,
	        modal      : false,
		loop       : true,
           helpers : {
	        title : {
		     type : 'inside', // outside , inside
			}
		}
	});							
 
$(".fancybox-media")
	.fancybox({
		openEffect : 'none',
		closeEffect : 'none',
		prevEffect : 'none',
		nextEffect : 'none',
	        arrows : false,
	helpers : {
		media : {},
		buttons : {}
		}
       });
});
</script>
 
<?php 
}
    wp_enqueue_style("jquery.fancybox",WP_PLUGIN_URL."/myfancybox/source/jquery.fancybox.css",false,"2.1.5");
    wp_enqueue_script("jquery");
    wp_enqueue_script("jquery.fancybox", WP_PLUGIN_URL."/myfancybox/source/jquery.fancybox.pack.js", array("jquery"), "2.1.5",1);
    wp_enqueue_script("jquery.fancyboxmousewheel", WP_PLUGIN_URL."/myfancybox/lib/jquery.mousewheel-3.0.6.pack.js", "3.0.6",1);	
    wp_enqueue_script("jquery.fancyboxmedia", WP_PLUGIN_URL."/myfancybox/source/helpers/jquery.fancybox-media.js", array("jquery"), "1.0.6",1);	
    add_action('wp_head', 'fancybox'); 
?>