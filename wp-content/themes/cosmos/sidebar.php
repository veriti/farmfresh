<?php 
	/* This file will render the sidebar, as defined by the user in the admin area */
?>

<div class="row">
	<div class="col-sm-12">
		<div class="formHolder_contact">
			<?php echo do_shortcode( '[contact-form-7 id="76" title="Contact Form"]' ); ?>
		</div>
	</div>
</div>

<?php // dynamic_sidebar( 'sidebar' ); ?>