<?php
	/* Blog, Search and Backup page template */
	get_header();
	get_template_part( 'partials/breadcrumbs' );
?>

<div id="content">
<div class="container">
	<div class="row">
		<div id="mainContent" class="<?php if( get_field('inner_post_sidebar', 'option') ){ ?>col-sm-8<?php } else { ?>col-sm-12<?php } ?><?php if( get_field('sidebar_left', 'option') ){ ?> pull-right<?php } ?>">

			<?php if ( is_search() ) { ?>
				<h1>Search Results</h1>
			<?php } ?>

			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

				<div class="custom_post_item">

					<article class="post">
					
						<h1 class="title">
							<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
								<?php the_title(); ?>
							</a>
						</h1>

						<?php if ( get_field('featured_image') && !$post->post_content=="" ) { ?>

							<div class="row the_content">

								<div class="col-lg-4 col-md-6 col-sm-12">
									<div class="featured_image">
										<?php $image = wp_get_attachment_image_src(get_field('featured_image'), 'full'); ?>
										<a href="<?php the_permalink(); ?>" rel="gallery">
											<img src="<?php echo $image[0]; ?>" />
										</a>
									</div>
								</div>

								<div class="col-lg-8 col-md-6 col-sm-12">
									<div class="post-meta">
										<?php the_time('m/d/Y'); ?> | 
										<?php if( comments_open() ) : ?>
											<span class="comments-link">
												<?php comments_popup_link( __( 'Comment', 'break' ), __( '1 Comment', 'break' ), __( '% Comments', 'break' ) ); ?>
											</span>
										<?php endif; ?>
									
									</div><!--/post-meta -->
									
									<div class="the-content">
										<?php the_content( 'Read More...' ); ?>
										
										<?php wp_link_pages(); ?>
									</div><!-- the-content -->

									<div class="meta clearfix">
										<div class="category"><?php echo get_the_category_list(); ?></div>
										<div class="tags"><?php echo get_the_tag_list( '| &nbsp;', '&nbsp;' ); ?></div>
									</div><!-- Meta -->
								</div>
							</div>

						<?php } else { ?>

							<div class="row the_content">
								<div class="col-sm-12">
									<div class="post-meta">
										<?php the_time('m/d/Y'); ?> | 
										<?php if( comments_open() ) : ?>
											<span class="comments-link">
												<?php comments_popup_link( __( 'Comment', 'break' ), __( '1 Comment', 'break' ), __( '% Comments', 'break' ) ); ?>
											</span>
										<?php endif; ?>
									
									</div><!--/post-meta -->
									
									<div class="the-content">
										<?php the_content( 'Read More...' ); ?>
										
										<?php wp_link_pages(); ?>
									</div><!-- the-content -->

									<div class="meta clearfix">
										<div class="category"><?php echo get_the_category_list(); ?></div>
										<div class="tags"><?php echo get_the_tag_list( '| &nbsp;', '&nbsp;' ); ?></div>
									</div><!-- Meta -->
								</div>
							</div>

						<?php } ?>						
					</article>
				</div><!-- custom_post_item -->

			<?php endwhile; ?>
				
				<!-- pagintation -->
				<div id="pagination" class="clearfix">
					<div class="past-page pull-right"><?php previous_posts_link( 'newer' ); ?></div>
					<div class="next-page pull-left"><?php next_posts_link( 'older' ); ?></div>
				</div><!-- pagination -->

			<?php  else : ?>

				<article class="post error">
					<h1 class="404">Nothing posted yet</h1>
				</article>

			<?php endif; ?>
		</div><!-- col-sm-8 -->

		<?php get_template_part( 'partials/sidebar_posts' ); ?>
	</div><!-- row -->
</div><!-- container -->
</div><!-- content -->

<?php get_footer(); ?>