<?php
/*-----------------------------------------------------------------------------------
/* This file will be referenced every time a template/page loads on your Wordpress site
/* This is the place to define custom fxns and specialty code
/*-----------------------------------------------------------------------------------*/

// Define the version so we can easily replace it throughout the theme
define( 'SLX_VERSION', 1.0 );

require_once('func/image_sizes.php');
require_once('func/menus.php');
require_once('wp_bootstrap_navwalker.php');
require_once('func/shortcodes.php');
require_once('func/jquery.php');
require_once('func/widgets.php');
require_once('func/acf_options.php');
require_once('func/cpt_showhide.php');
require_once('func/tiny_mce.php');


/*-----------------------------------------------------------------------------------
	LESS
-----------------------------------------------------------------------------------*/

require_once( 'wp-less/wp-less.php' );

// pass variables into all .less files
function my_less_vars( $vars, $handle ) {

	/*if (get_field('branding_primary', 'option')):
		$vars[ 'branding_primary' ] = get_field('branding_primary','option');
	endif;
	if (get_field('branding_primary_light', 'option')):
		$vars[ 'branding_primary_light' ] = get_field('branding_primary_light','option');
	endif;
	if (get_field('branding_primary_dark', 'option')):
		$vars[ 'branding_primary_dark' ] = get_field('branding_primary_dark','option');
	endif;

	if (get_field('branding_secondary', 'option')):
		$vars[ 'branding_secondary' ] = get_field('branding_secondary','option');
	endif;
	if (get_field('branding_secondary_light', 'option')):
		$vars[ 'branding_secondary_light' ] = get_field('branding_secondary_light','option');
	endif;
	if (get_field('branding_secondary_dark', 'option')):
		$vars[ 'branding_secondary_dark' ] = get_field('branding_secondary_dark','option');
	endif;

	if (get_field('branding_tertiary', 'option')):
		$vars[ 'branding_tertiary' ] = get_field('branding_tertiary','option');
	endif;
	if (get_field('branding_tertiary_light', 'option')):
		$vars[ 'branding_tertiary_light' ] = get_field('branding_tertiary_light','option');
	endif;
	if (get_field('branding_tertiary_dark', 'option')):
		$vars[ 'branding_tertiary_dark' ] = get_field('branding_tertiary_dark','option');
	endif;
	if (get_field('blah_colour', 'option')):
		$vars[ 'blah_colour' ] = get_field('blah_colour','option');
	endif;
	if (get_field('blah_colour_light', 'option')):
		$vars[ 'blah_colour_light' ] = get_field('blah_colour_light','option');
	endif;
	if (get_field('blah_colour_dark', 'option')):
		$vars[ 'blah_colour_dark' ] = get_field('blah_colour_dark','option');
	endif;*/
	if (get_field('branding_primary', 'option')):
		$vars[ 'branding_primary' ] = get_field('branding_primary','option');
	endif;
	if (get_field('branding_secondary', 'option')):
		$vars[ 'branding_secondary' ] = get_field('branding_secondary','option');
	endif;
	if (get_field('branding_tertiary', 'option')):
		$vars[ 'branding_tertiary' ] = get_field('branding_tertiary','option');
	endif;

	return $vars;
}
add_filter( 'less_vars', 'my_less_vars', 10, 2 );

// Clear the Less Cache
function clear_wp_less_cache() {

	$uploads = wp_upload_dir();

	$childstylecss = $uploads['basedir'].'/wp-less-cache/child-style.css';
	$childstylecsscache = $uploads['basedir'].'/wp-less-cache/child-style.css.cache';
	unlink($childstylecss);
	unlink($childstylecsscache);

	touch(get_stylesheet_directory().'/less/style.less');
	touch(get_stylesheet_directory().'/less/custom.less');

}

// Recompile LESS when you save the ACF Options page
function my_acf_save_post( $post_id ) {

	// Write to parent theme's custom less file
	$customless_file = get_stylesheet_directory().'/less/custom.less';
	file_put_contents($customless_file,get_field('custom_less','option'));

	clear_wp_less_cache();
	
}
add_action('acf/save_post', 'my_acf_save_post', 9999);


// Clear cache when switching themes
add_action('after_switch_theme', 'clear_wp_less_cache');


/*-----------------------------------------------------------------------------------
	Enqueue Styles and Scripts
-----------------------------------------------------------------------------------*/

function slx_scripts()  { 

	wp_register_script( 'slx-bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js', array('jquery'), true );
	wp_register_script( 'slx-cycle2', get_template_directory_uri() . '/js/jquery.cycle2.min.js', array('jquery'), '20130404', true );
	wp_register_script( 'slx-matchheight', get_template_directory_uri() . '/js/jquery.matchHeight-min.js', array('jquery'), true );
	wp_register_script( 'slx-script', get_template_directory_uri() . '/js/script.js', array('jquery'), true );
	wp_enqueue_script( 'slx-bootstrap' );
	wp_enqueue_script( 'slx-cycle2' );
	wp_enqueue_script( 'slx-matchheight' );
	wp_enqueue_script( 'slx-script' );
  
}
add_action( 'wp_enqueue_scripts', 'slx_scripts' );

?>