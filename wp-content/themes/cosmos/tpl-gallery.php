<?php 
	/* Template Name: Gallery Page */
	get_header();
	get_template_part( 'partials/breadcrumbs' );
?>

<div id="content">
<div class="container">
	<div class="row">
		<div id="mainContent" class="<?php if( get_field('inner_page_sidebar', 'option') ){ ?>col-sm-8<?php } else { ?>col-sm-12<?php } ?><?php if( get_field('sidebar_left', 'option') ){ ?> pull-right<?php } ?>">
			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
		
				

				<?php if ( get_field('featured_image') && !$post->post_content=="" ) { ?>

					<div class="row the_content">
						<?php if ( get_field('featured_image_type')=='featured_long' ) { ?>

							<div class="col-sm-12">
								<div class="featured_image">
									<?php $image = wp_get_attachment_image_src(get_field('featured_image'), 'cpt_banner_image'); ?>
									<?php $imageF = wp_get_attachment_image_src(get_field('featured_image'), 'full'); ?>
									<a href="<?php echo $imageF[0]; ?>" rel="gallery">
										<img src="<?php echo $image[0]; ?>" />
									</a>
								</div>
							</div>
							<div class="col-sm-12">
								<?php the_content(); ?>
							</div>

						<?php } elseif ( get_field('featured_image_type')=='featured_short' ) { ?>

							<div class="col-lg-4 col-md-6 col-sm-12">
								<div class="featured_image">
									<?php $image = wp_get_attachment_image_src(get_field('featured_image'), 'full'); ?>
									<a href="<?php echo $image[0]; ?>" rel="gallery">
										<img src="<?php echo $image[0]; ?>" />
									</a>
								</div>
							</div>
							<div class="col-lg-8 col-md-6 col-sm-12">
								<?php the_content(); ?>
							</div>

						<?php } ?>
					</div>

				<?php } elseif ( get_field('featured_image') && $post->post_content=="" ) { ?>

					<div class="row the_content">
						<div class="col-sm-12">
							<div class="featured_image">
								<?php $image = wp_get_attachment_image_src(get_field('featured_image'), 'cpt_banner_image'); ?>
								<?php $imageF = wp_get_attachment_image_src(get_field('featured_image'), 'full'); ?>
								<a href="<?php echo $imageF[0]; ?>" rel="gallery">
									<img src="<?php echo $image[0]; ?>" />
								</a>
							</div>
						</div>
					</div>

				<?php } elseif ( !$post->post_content=="" ) { ?>

					<div class="row the_content">
						<div class="col-sm-12">
							<?php the_content(); ?>
						</div>
					</div>

				<?php } ?>
		
				<?php if (get_field('gallery')): ?>
					<div class="row the_content">
						<div class="col-sm-12">
							<?php if (get_field('gallery_title')){ ?>
								<h3><?php the_field('gallery_title'); ?></h3>
							<?php } ?>
							<div class="gallery_holder">
								<?php
									$images = get_field('gallery');
									if( $images ):
									foreach( $images as $image ):
								?>
									<a href="<?php echo $image['url']; ?>" rel="gallery">
										<img src="<?php echo $image['sizes']['ratio3-2']; ?>" alt="<?php echo $image['alt']; ?>" />
									</a>
								<?php endforeach; endif; ?>
							</div><!-- galleryHolder -->
						</div>
					</div>
				<?php endif; ?>

			<?php endwhile; else : ?>

				<article class="post error">
					<h1 class="404">No Pages have been posted yet</h1>
				</article>

			<?php endif; ?>
		</div><!-- col-sm-8 -->

		<?php if( get_field('inner_page_sidebar', 'option') ){ ?>
			<div id="sidebar" class="col-sm-4<?php if( get_field('sidebar_left', 'option') ){ ?> pull-left<?php } ?>">
				<?php get_sidebar(); ?>
			</div><!-- col-sm-4 -->
		<?php } ?>
	</div><!-- row -->
</div><!-- container -->
</div><!-- content -->

<?php
	if ( wp_get_theme() != 'Venus' ) {
		if( get_field('services_box', 'option') ){
			get_template_part( 'partials/box_links_holder' );
		}
	}
?>

<?php get_footer(); ?>