<?php 
	/* Template Name: Home Page */
	get_header();
	if ( wp_get_theme() != 'Venus' ) {
		get_template_part( 'partials/box_links_holder' );
	}
?>

<div id="content">
<div class="container">
	<div class="row">
		<div id="mainContent" class="<?php if( get_field('inner_page_sidebar', 'option') ){ ?>col-sm-8<?php } else { ?>col-sm-12<?php } ?><?php if( get_field('sidebar_left', 'option') ){ ?> pull-right<?php } ?>">
			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
		
				<div class="row">
					<div class="col-sm-12">
						<?php the_content(); ?>
					</div>
				</div>

			<?php endwhile; else : ?>

				<article class="post error">
					<h1 class="404">Nothing posted yet</h1>
				</article>

			<?php endif; ?>
		</div><!-- col-sm-8 -->

		<?php if( get_field('inner_page_sidebar', 'option') ){ ?>
			<div id="sidebar" class="col-sm-4<?php if( get_field('sidebar_left', 'option') ){ ?> pull-left<?php } ?>">
				<?php get_sidebar(); ?>
			</div><!-- col-sm-4 -->
		<?php } ?>
	</div><!-- row -->
</div><!-- container -->
</div><!-- content -->

<?php get_footer(); ?>