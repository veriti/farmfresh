<?php 
	/* The template for displaying the 404 Error page. */
	get_header();
	get_template_part( 'partials/breadcrumbs' );
?>

<div id="content">
<div class="container">
	<div class="row">
		<div id="mainContent" class="<?php if( get_field('inner_page_sidebar', 'option') ){ ?>col-sm-8<?php } else { ?>col-sm-12<?php } ?><?php if( get_field('sidebar_left', 'option') ){ ?> pull-right<?php } ?>">

			<div class="row">
				<div class="col-sm-12">
					<h1>This page does not exist</h1>
					<p>Please use the navigation menu to find the page you were looking for.</p>
				</div>
			</div>

		</div><!-- col-sm-8 -->

		<?php if( get_field('inner_page_sidebar', 'option') ){ ?>
			<div id="sidebar" class="col-sm-4<?php if( get_field('sidebar_left', 'option') ){ ?> pull-left<?php } ?>">
				<?php get_sidebar(); ?>
			</div><!-- col-sm-4 -->
		<?php } ?>
	</div><!-- row -->
</div><!-- container -->
</div><!-- content -->

<?php
	if ( wp_get_theme() != 'Venus' ) {
		if( get_field('services_box', 'option') ){
			get_template_part( 'partials/box_links_holder' );
		}
	}
?>

<?php get_footer(); ?>