<?php
	/*-----------------------------------------------------------------------------------*/
	/* This template will be called by all other template files to finish 
	/* rendering the page and display the footer area/content
	/*-----------------------------------------------------------------------------------*/
?>

<?php
	get_template_part( 'partials/footer_above' );
	if ( wp_get_theme() != 'Jupiter' ) {
		get_template_part( 'partials/footer_main' );
	}
?>

<div id="below_footer">
<div class="container">
	<div class="row">
		<div class="col-sm-12">
			<p>Copyright 2015 <?php bloginfo('name'); ?> | <?php the_field('footer_built_by', 'option'); ?></p>
		</div>
	</div>
</div><!-- container -->
</div><!-- below_footer -->

<?php 
	if( get_field('featured_button', 'option')){ 
		if (get_field('page_or_form', 'option') == 'link_to_form'){ 
			if (get_field('featured_button_form', 'option')){ 
?>
	<div style="display:none;">
		<div id="popup_form" class="formHolder_popup">
			<?php echo do_shortcode( get_field('featured_button_form', 'option')); ?>
		</div>
	</div>
<?php }}} ?>

<div id="overlay"></div>

<?php wp_footer(); ?>

</div><!-- wrapper -->
</body>
</html>