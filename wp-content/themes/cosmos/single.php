<?php
	/* The template for displaying any single post. */
	get_header();
	get_template_part( 'partials/breadcrumbs' );
?>

<div id="content">
<div class="container">
	<div class="row">
		<div id="mainContent" class="<?php if( get_field('inner_post_sidebar', 'option') ){ ?>col-sm-8<?php } else { ?>col-sm-12<?php } ?><?php if( get_field('sidebar_left', 'option') ){ ?> pull-right<?php } ?>">
			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

				<div class="row">
					<div class="col-sm-12">
						<h1><?php the_title(); ?></h1>
					</div>
				</div>

				<?php if ( get_field('featured_image') && !$post->post_content=="" ) { ?>

					<div class="row the_content">
						<?php if ( get_field('featured_image_type')=='featured_long' ) { ?>

							<div class="col-sm-12">
								<div class="featured_image">
									<?php $image = wp_get_attachment_image_src(get_field('featured_image'), 'cpt_banner_image'); ?>
									<?php $imageF = wp_get_attachment_image_src(get_field('featured_image'), 'full'); ?>
									<a href="<?php echo $imageF[0]; ?>" rel="gallery">
										<img src="<?php echo $image[0]; ?>" />
									</a>
								</div>
							</div>
							<div class="col-sm-12">
								<?php the_content(); ?>
							</div>

						<?php } elseif ( get_field('featured_image_type')=='featured_short' ) { ?>

							<div class="col-lg-4 col-md-6 col-sm-12">
								<div class="featured_image">
									<?php $image = wp_get_attachment_image_src(get_field('featured_image'), 'full'); ?>
									<a href="<?php echo $image[0]; ?>" rel="gallery">
										<img src="<?php echo $image[0]; ?>" />
									</a>
								</div>
							</div>
							<div class="col-lg-8 col-md-6 col-sm-12">
								<?php the_content(); ?>
							</div>

						<?php } ?>
					</div>

				<?php } elseif ( get_field('featured_image') && $post->post_content=="" ) { ?>

					<div class="row the_content">
						<div class="col-sm-12">
							<div class="featured_image">
								<?php $image = wp_get_attachment_image_src(get_field('featured_image'), 'cpt_banner_image'); ?>
								<?php $imageF = wp_get_attachment_image_src(get_field('featured_image'), 'full'); ?>
								<a href="<?php echo $imageF[0]; ?>" rel="gallery">
									<img src="<?php echo $image[0]; ?>" />
								</a>
							</div>
						</div>
					</div>

				<?php } elseif ( !$post->post_content=="" ) { ?>

					<div class="row the_content">
						<div class="col-sm-12">
							<?php the_content(); ?>
							<?php if ( is_singular( 'testimonial' ) ) { ?>
							    <p class="fw_500">- <?php the_title(); ?></p>
							<?php } ?>
						</div>
					</div>

				<?php } ?>

				<?php if( have_rows('tabs') ): ?>
					<div class="row the_content">
					<div class="col-sm-12">
					<div class="tabs">
						<ul class="nav nav-tabs" data-tabs="tabs">
							<?php while ( have_rows('tabs') ) : the_row(); ?>
								<li>
									<a href="#<?php echo str_replace(' ', '_', get_sub_field('title')); ?>" data-toggle="tab" role="tab">
										<?php the_sub_field('title'); ?>
									</a>
								</li>
							<?php endwhile; ?>
						</ul>

						<div class="tab-content">
							<?php while(has_sub_field('tabs')): ?>
								<?php if( get_row_layout() == 'content_tab' ): ?>

									<div class="tab-pane fade <?php echo str_replace(' ', '_', get_sub_field('title')); ?>" id="<?php echo str_replace(' ', '_', get_sub_field('title')); ?>" id="<?php echo str_replace(' ', '_', get_sub_field('title')); ?>">
										<?php if (get_sub_field('title')){ ?>
											<div class="row">
												<div class="col-sm-12">
													<h3><?php the_sub_field('title'); ?></h3>
													<?php the_sub_field('content'); ?>
												</div>
											</div>
										<?php } ?>
									</div>

								<?php elseif( get_row_layout() == 'gallery_tab' ): ?>

									<div class="tab-pane fade <?php echo str_replace(' ', '_', get_sub_field('title')); ?>" id="<?php echo str_replace(' ', '_', get_sub_field('title')); ?>" id="<?php echo str_replace(' ', '_', get_sub_field('title')); ?>">
										<?php if (get_sub_field('title')){ ?>
											<div class="row">
												<div class="col-sm-12"><h3><?php the_sub_field('title'); ?></h3></div>
											</div>
											<div class="row">
												<div class="col-sm-12">
													<div class="gallery_holder">
														<?php
															$images = get_field('gallery');
															if( $images ):
															foreach( $images as $image ):
														?>
															<a href="<?php echo $image['url']; ?>" rel="gallery">
																<img src="<?php echo $image['sizes']['ratio3-2']; ?>" alt="<?php echo $image['alt']; ?>" />
															</a>
														<?php endforeach; endif; ?>
													</div>
												</div>
											</div>
										<?php } ?>
									</div>

								<?php endif; ?>
							<?php endwhile; ?>
						</div>
					</div><!-- tabs -->
					</div>
					</div>
				<?php endif; ?>
		
				<?php if (get_field('gallery')): ?>
					<div class="row the_content">
						<div class="col-sm-12">
							<?php if (get_field('gallery_title')){ ?>
								<h3><?php the_field('gallery_title'); ?></h3>
							<?php } ?>
							<div class="gallery_holder">
								<?php
									$images = get_field('gallery');
									if( $images ):
									foreach( $images as $image ):
								?>
									<a href="<?php echo $image['url']; ?>" rel="gallery">
										<img src="<?php echo $image['sizes']['ratio3-2']; ?>" alt="<?php echo $image['alt']; ?>" />
									</a>
								<?php endforeach; endif; ?>
							</div><!-- galleryHolder -->
						</div>
					</div>
				<?php endif; ?>

				<?php /*
				<div class="row">
					<div class="col-sm-12">
						<?php wp_link_pages(); ?>
					</div>
				</div>
				*/ ?>

				<div class="meta clearfix">
					<div class="category"><?php echo get_the_category_list(); // Display the categories this post belongs to, as links ?></div>
					<div class="tags"><?php echo get_the_tag_list( '| &nbsp;', '&nbsp;' ); // Display the tags this post has, as links separated by spaces and pipes ?></div>
				</div><!-- Meta -->

			<?php endwhile; else : ?>

				<article class="post error">
					<h1 class="404">Nothing posted yet</h1>
				</article>

			<?php endif; ?>
		</div><!-- col-sm-8 -->

		<?php get_template_part( 'partials/sidebar_posts' ); ?>
	</div><!-- row -->
</div><!-- container -->
</div><!-- content -->

<?php get_footer(); ?>

<?php /*if ( is_singular( 'catalogue' ) ) { ?>
    <p>catalogue</p>
<?php } else { ?>
    <p>not catalogue</p>
<?php } */?>