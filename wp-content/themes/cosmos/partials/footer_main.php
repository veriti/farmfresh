<footer>
<div class="container">
	<div class="row">
		<div class="col-sm-4 footer_1 footer_links">
			<h3>Quick Links</h3>
			<?php wp_nav_menu( array( 'theme_location' => 'secondary', 'menu_class' => 'nav-menu' ) ); ?>
		</div>
		<div class="col-sm-5 footer_2 footer_contact">
			<h3>Contact us</h3>
			<?php if (get_field('phone_number', 'option')): ?>
				<div class="row">
					<div class="col-lg-3 col-md-4 col-sm-5">Telephone:</div>
					<div class="col-lg-9 col-md-8 col-sm-7 footer_contact_right"><a href="tel:<?php the_field('phone_number', 'option'); ?>"><?php the_field('phone_number', 'option'); ?></a></div>
				</div>
			<?php endif; ?>
			<?php if (get_field('phone_number_2', 'option')): ?>
				<div class="row">
					<div class="col-lg-3 col-md-4 col-sm-5">Telephone 2:</div>
					<div class="col-lg-9 col-md-8 col-sm-7 footer_contact_right"><a href="tel:<?php the_field('phone_number_2', 'option'); ?>"><?php the_field('phone_number_2', 'option'); ?></a></div>
				</div>
			<?php endif; ?>
			<?php if (get_field('mobile_number', 'option')): ?>
				<div class="row">
					<div class="col-lg-3 col-md-4 col-sm-5">Mobile:</div>
					<div class="col-lg-9 col-md-8 col-sm-7 footer_contact_right"><a href="tel:<?php the_field('mobile_number', 'option'); ?>"><?php the_field('mobile_number', 'option'); ?></a></div>
				</div>
			<?php endif; ?>
			<?php if (get_field('fax_number', 'option')): ?>
				<div class="row">
					<div class="col-lg-3 col-md-4 col-sm-5">Fax:</div>
					<div class="col-lg-9 col-md-8 col-sm-7 footer_contact_right"><a href="tel:<?php the_field('fax_number', 'option'); ?>"><?php the_field('fax_number', 'option'); ?></a></div>
				</div>
			<?php endif; ?>
			<?php if (get_field('email_address', 'option')): ?>
				<div class="row">
					<div class="col-lg-3 col-md-4 col-sm-5">Email:</div>
					<div class="col-lg-9 col-md-8 col-sm-7 footer_contact_right"><a class="email_address" href="mailto:<?php the_field('email_address', 'option'); ?>"><?php the_field('email_address', 'option'); ?></a></div>
				</div>
			<?php endif; ?>
			<?php if (get_field('address', 'option')): ?>
				<div class="row">
					<div class="col-lg-3 col-md-4 col-sm-5">Address:</div>
					<div class="col-lg-9 col-md-8 col-sm-7 footer_contact_right"><?php the_field('address', 'option'); ?></div>
				</div>
			<?php endif; ?>			
		</div>
		<div class="col-sm-3 footer_3 footer_terms">
			<?php //dynamic_sidebar( 'footer' ); ?>
			<h3>Terms of Use</h3>
			<p>
				&copy; Copyright <?php bloginfo('name'); ?><br>
				All rights reserved.<br>
				<a href="/privacy-policy/">Privacy Policy </a>
			</p>
		</div>
	</div>
</div><!-- container -->
</footer>