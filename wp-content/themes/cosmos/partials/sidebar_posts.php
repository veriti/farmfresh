<?php
	$post_type = get_post_type();
	if ( $post_type )
	{
		$post_type_data = get_post_type_object( $post_type );
		$post_type_slug = $post_type_data->rewrite['slug'];
		$object = get_post_type_object( $post_type_slug );
	}
	// $object = get_post_type_object( 'staff' );
	// echo $object->labels->name;
?>

<?php if( get_field('inner_post_sidebar', 'option') ){ ?>
	<div id="sidebar" class="col-sm-4<?php if( get_field('sidebar_left', 'option') ){ ?> pull-left<?php } ?>">
		<div class="row">
			<div class="col-sm-12">

				<div class="sidebar_block">
					<h2>Search <?php echo $object->label; ?></h2>
					<div class="sidebar_block_inner post_search">
						<form action="/" class="searchform" id="searchform" method="get" role="search">
							<?php if ( get_post_type() == 'post' ) { ?>
								<input type="hidden" name="post_type" value="post" />
							<?php } else { ?>
								<input type="hidden" name="post_type" value="<?php echo $post_type_slug; ?>" />
							<?php } ?>			
							<label for="s" class="screen-reader-text hidelabel">Search...</label>
							<button id="searchsubmit" type="submit">
								<i class="fa fa-search"></i>
							</button>
							<!-- <input type="text" id="s" name="s" value="<?//=$_GET['s'];?>" class="haslabel"> -->
							<span><input type="text" id="s" name="s" value="" class="haslabel"></span>
						</form>
					</div>
				</div>

				<div class="sidebar_block">
					<h2>Recent <?php echo $object->label; ?></h2>
					<div class="sidebar_block_inner">
						<ul>
						<?php 
							$args = array(
								'showposts' => 5,
								'post_type' => $post_type_slug
							);
							$loop = new WP_Query( $args );
							while ( $loop->have_posts() ) : $loop->the_post();
							/*$content = get_the_content();
							$trimmed_content = wp_trim_words( $content, 35 );*/
						?>
							<li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
						<?php
							endwhile;
							wp_reset_query();
						?>
						</ul>
					</div>
				</div>

				<?php if ( get_post_type() == 'post' ) { ?>
					<div class="sidebar_block">
						<h2>Categories</h2>
						<div class="sidebar_block_inner">
							<ul>
								<?php wp_list_categories('title_li='); ?>
							</ul>
						</div>
					</div>
				<?php } ?>

			</div>
		</div>
	</div><!-- col-sm-4 -->
<?php } ?>