<?php if( get_field('breadcrumbs', 'option') ){ ?>
<div id="breadcrumbs">
	<div class="container">
		<div class="row-fluid">
			<div class="span12">
				<?php if(function_exists('bcn_display')) { bcn_display(); } ?>
			</div><!-- span12 -->
		</div><!-- row-fluid -->
	</div><!-- container -->
</div><!-- breadcrumbs -->
<?php } ?>