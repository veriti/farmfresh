<?php if (get_field('box_link_title', 'option')){ ?>
	<h2><?php the_field('box_link_title', 'option'); ?></h2>
<?php } ?>

<div class="row">
	<?php if (get_field('box_link_type', 'option') == 'box_link_type_select') { ?>

		<?php
			$post_objects = get_field('box_link_select', 'option');
			if( $post_objects ):
			foreach( $post_objects as $post):
			setup_postdata($post);
		?>
			<div class="<?php the_field('box_link_columns','option'); ?>">
				<a class="box_link" href="<?php echo get_permalink(); ?>">
					<span class="box_link_image">
						<?php if (get_field('featured_image')){ ?>
							<?php
								if ( wp_get_theme() == 'Jupiter' ){
									$image = wp_get_attachment_image_src(get_field('featured_image'), 'ratio2-1');
								} else if ( wp_get_theme() == 'Neptune' ){
									$image = wp_get_attachment_image_src(get_field('featured_image'), 'ratio3-2');
								}
							?>
							<img src="<?php echo $image[0]; ?>" alt="<?php the_title(); ?>" />
						<?php } else { ?>
							<?php
								if ( wp_get_theme() == 'Jupiter' ){
									$image = wp_get_attachment_image_src(get_field('backup_image','option'), 'ratio2-1');
								} else if ( wp_get_theme() == 'Neptune' ){
									$image = wp_get_attachment_image_src(get_field('backup_image','option'), 'ratio3-2');
								}
							?>
							<img src="<?php echo $image[0]; ?>" alt="<?php the_title(); ?>" />
						<?php } ?>
					</span>
					<span class="box_link_title"><?php the_title(); ?></span>
					<?php /*if (get_field('show_box_link_blurb', 'option')){ ?>
						<?php if (get_field('short_description')){ ?>
							<span class="box_link_blurb"><?php the_field('short_description'); ?></span>
						<?php } else { ?>
							<span class="box_link_blurb">
								<?php
									$content = get_the_content();
									$trimmed_content = wp_trim_words( $content, 25 );
									echo $trimmed_content;
								?>
							</span>
						<?php } ?>
					<?php } */?>
				</a>
			</div>
		<?php
			endforeach;
			wp_reset_postdata();
			endif;
		?>
	
	<?php } else if (get_field('box_link_type', 'option') == 'box_link_type_create') { ?>

		<?php if(get_field('box_link_create','option')): ?>
			<?php while(has_sub_field('box_link_create','option')): ?>
				<div class="<?php the_field('box_link_columns','option'); ?>">
					<?php if (get_sub_field('page_link','option')) { ?>
						<a class="box_link" href="<?php the_sub_field('page_link','option'); ?>"<?php if (get_sub_field('new_tab','option')) { ?> target="_blank"<?php } ?>>
					<?php } else if (get_sub_field('page_url','option')) { ?>
						<a class="box_link" href="<?php the_sub_field('page_url','option'); ?>"<?php if (get_sub_field('new_tab','option')) { ?> target="_blank"<?php } ?>>
					<?php } else { ?>
						<span class="box_link">
					<?php } ?>
						<span class="box_link_image">
							<?php if (get_sub_field('image','option')){ ?>
								<?php $image = wp_get_attachment_image_src(get_sub_field('image','option'), 'ratio3-2'); ?>
								<img src="<?php echo $image[0]; ?>" alt="<?php the_title(); ?>" />
							<?php } else { ?>
								<?php $image = wp_get_attachment_image_src(get_field('backup_image','option'), 'ratio3-2'); ?>
								<img src="<?php echo $image[0]; ?>" alt="<?php the_title(); ?>" />
							<?php } ?>
						</span>
						<?php if (get_sub_field('title','option')){ ?>
							<span class="box_link_title"><?php the_sub_field('title','option'); ?></span>
						<?php } ?>
						<?php /* if (get_sub_field('box_link_blurb')){ ?>
							<span class="box_link_blurb"><?php the_sub_field('box_link_blurb'); ?></span>
						<?php } */ ?>
					<?php if ( get_sub_field('page_link','option') || get_sub_field('page_url','option')){ ?>
						</a>
					<?php } else { ?>
						</span>
					<?php } ?>
				</div>
			<?php endwhile; ?>
		<?php endif; ?>

	<?php } // box_link_type ?>

</div><!-- row -->