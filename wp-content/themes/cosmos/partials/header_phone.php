<?php if (get_field('phone_number', 'option')): ?>
	<span id="head_phone">
		<span id="head_social">
			<?php if( get_field('icon_in_header_facebook', 'option') ){ ?>
				<?php if (get_field('facebook_url', 'option')): ?>
					<a target="_blank" href="<?php the_field('facebook_url', 'option'); ?>"><i class="fa fa-facebook-square"></i></a>
				<?php endif; ?>
			<?php } ?>
			<?php if( get_field('icon_in_header_twitter', 'option') ){ ?>
				<?php if (get_field('twitter_url', 'option')): ?>
					<a target="_blank" href="<?php the_field('twitter_url', 'option'); ?>"><i class="fa fa-twitter-square"></i></a>
				<?php endif; ?>
			<?php } ?>
			<?php if( get_field('icon_in_header_google', 'option') ){ ?>
				<?php if (get_field('google_plus_url', 'option')): ?>
					<a target="_blank" href="<?php the_field('google_plus_url', 'option'); ?>"><i class="fa fa-google-plus-square"></i></a>
				<?php endif; ?>
			<?php } ?>
			<?php if( get_field('icon_in_header_linkedin', 'option') ){ ?>
				<?php if (get_field('linkedin_url', 'option')): ?>
					<a target="_blank" href="<?php the_field('linkedin_url', 'option'); ?>"><i class="fa fa-linkedin-square"></i></a>
				<?php endif; ?>
			<?php } ?>
			<?php if( get_field('icon_in_header_youtube', 'option') ){ ?>
				<?php if (get_field('youtube_url', 'option')): ?>
					<a target="_blank" href="<?php the_field('youtube_url', 'option'); ?>"><i class="fa fa-youtube-square"></i></a>
				<?php endif; ?>
			<?php } ?>
		</span>
		<?php if (get_field('phone_preface', 'option') == 'phone_preface_icon') { ?>
			<span id="head_phone_icon"><i class="fa fa-phone"></i> </span>
		<?php } else if (get_field('phone_preface', 'option') == 'phone_preface_text') { ?>
			<span id="head_phone_text"><?php the_field('phone_preface_text', 'option'); ?> </span>
		<?php } ?>
		<a class="<?php the_field('phone_number_colour', 'option'); ?>" href="tel:<?php the_field('phone_number', 'option'); ?>"><?php the_field('phone_number', 'option'); ?></a>
	</span>
<?php endif; ?>