<?php if( get_field('show_email', 'option') ){ ?>

	<?php if (get_field('email_address', 'option')): ?>
		<span id="head_email">
			<a class="<?php the_field('email_font_weight', 'option'); ?> <?php the_field('email_colour', 'option'); ?>" href="mailto:<?php the_field('email_address', 'option'); ?>">
				<?php the_field('email_address', 'option'); ?>
			</a>
		</span>
	<?php endif; ?>

<?php } ?>