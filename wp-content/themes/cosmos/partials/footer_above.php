<?php if (get_field('af_content_type', 'option')): ?>
<div id="above_footer" class="<?php the_field('above_footer_background_type', 'option'); ?> <?php the_field('af_borders', 'option'); ?><?php if(get_field('af_borders', 'option') == 'af_border_top'){ ?>	<?php the_field('af_border_top_colour', 'option'); ?><?php } ?><?php if(get_field('af_borders', 'option') == 'af_border_btm'){ ?> <?php the_field('af_border_bottom_colour', 'option'); ?><?php } ?><?php if(get_field('af_borders', 'option') == 'af_border_topbtm'){ ?> <?php the_field('af_border_top_colour', 'option'); ?> <?php the_field('af_border_bottom_colour', 'option'); ?><?php } ?>" style="<?php if(get_field('above_footer_background_type', 'option') == 'af_bg_header'){ ?> <?php if(get_field('header_background_type', 'option') == 'header_background_type_colour'){ ?> background-color: <?php the_field('header_background_colour', 'option'); ?>;<?php } ?><?php if(get_field('header_background_type', 'option') == 'header_background_type_pattern'){ ?> background-image: url('<?php the_field('header_background_pattern', 'option'); ?>');<?php } ?><?php } ?><?php if(get_field('above_footer_background_type', 'option') == 'af_bg_colour'){ ?> background-color: <?php the_field('above_footer_background_colour', 'option'); ?>;<?php } ?><?php if(get_field('above_footer_background_type', 'option') == 'af_bg_pattern'){ ?> background-image: url('<?php the_field('above_footer_background_pattern', 'option'); ?>');<?php } ?><?php if(get_field('af_borders', 'option') == 'af_border_top'){ ?> border-top-width: <?php the_field('af_border_top_width', 'option'); ?>px;<?php } ?><?php if(get_field('af_borders', 'option') == 'af_border_btm'){ ?> border-bottom-width: <?php the_field('af_border_bottom_width', 'option'); ?>px;<?php } ?><?php if(get_field('af_borders', 'option') == 'af_border_topbtm'){ ?> border-top-width: <?php the_field('af_border_top_width', 'option'); ?>px; border-bottom-width: <?php the_field('af_border_bottom_width', 'option'); ?>px;<?php } ?>">
<div class="container">
	<div class="row">
		<div class="col-sm-12">

			<?php if (get_field('af_title', 'option')): ?>
				<h3><?php the_field('af_title', 'option'); ?></h3>
			<?php endif; ?>

			<?php if( have_rows('af_content_type', 'option') ): while ( have_rows('af_content_type', 'option') ) : the_row(); ?>
				<?php if( get_row_layout() == 'content' ): ?>

					<?php the_sub_field('af_content', 'option'); ?>

				<?php endif; // gallery ?>

				<?php if( get_row_layout() == 'gallery' ): ?>

					<div class="af_gallery">
						<?php
							$images = get_sub_field('af_gallery', 'option');
							if( $images ):
							foreach( $images as $image ):
						?>
							<div class="af_gallery_item <?php the_sub_field('af_columns', 'option'); ?>"><img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" /></div>
						<?php endforeach; endif; ?>
					</div>

				<?php endif; // gallery ?>

				<?php if( get_row_layout() == 'testimonials' ): ?>

					<div class="row">
					<div class="col-md-10 col-sm-12 col-md-offset-1 col-sm-offset-0">
					<div class="af_testimonials">

						<?php if (get_sub_field('testimonial', 'option')): ?>

							<?php
								$post_objects = get_sub_field('testimonial');
								if( $post_objects ):
								foreach( $post_objects as $post): // variable must be called $post (IMPORTANT)
								setup_postdata($post);
								$limit_words = get_sub_field('limit_words');
								$content = get_the_content();
								$trimmed_content = wp_trim_words( $content, $limit_words );
							?>
								<div class="af_testimonial">
									<p>
										"<?php echo $trimmed_content; ?>"<br />
										<span>- <?php the_title(); ?></span>
									</p>
								</div>
							<?php
								endforeach;
								wp_reset_postdata();
								endif;
							?>
					
						<?php else: ?>

							<?php 
								$args = array('post_type' => 'testimonial');
								$loop = new WP_Query( $args );
								while ( $loop->have_posts() ) : $loop->the_post();
								$limit_words = get_sub_field('limit_words');
								$content = get_the_content();
								$trimmed_content = wp_trim_words( $content, $limit_words );
							?>
								<div class="af_testimonial">
									<p>
										"<?php echo $trimmed_content; ?>"<br />
										<span>- <?php the_title(); ?></span>
									</p>
								</div>
							<?php endwhile;?>

						<?php endif; ?>

					</div><!-- af_testimonials -->
					</div>
					</div>

				<?php endif; // testimonials ?>
			<?php endwhile; else : endif; // af_content_type ?>

		</div>
	</div>
</div><!-- container -->
</div><!-- above_footer -->
<?php endif; ?>