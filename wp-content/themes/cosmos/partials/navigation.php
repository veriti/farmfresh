<div id="navigation" class="<?php if( get_field('nav_bottom_border', 'option')){ ?> nav_bottom_border<?php } ?><?php if( get_field('navigation_font_style', 'option')){ ?> <?php the_field('navigation_font_style', 'option'); ?><?php } ?>"<?php if( get_field('nav_bottom_border', 'option') && get_field('nav_bottom_border_height', 'option')){ ?> style="border-width: <?php the_field('nav_bottom_border_height', 'option'); ?>px"<?php } ?>>
<div class="container">
	<div class="row">
			
		<div class="col-sm-12 nav_flex <?php the_field('navigation_font', 'option'); ?>">
			<!-- <a class="btn_menu collapsed bc_primary_dark" data-target="#navclose" data-toggle="collapse">Menu</a> -->
			<div class="sidenav_close"><i class="fa fa-times"></i></div>
				
			<div class="nav_flex_menu <?php the_field('nav_menu_width', 'option'); ?>">
				<?php
					wp_nav_menu( array(
						'menu'				=> 'primary',
						'theme_location'	=> 'primary',
						'depth'				=> 6,
						'container'			=> 'div',
						'container_class'	=> 'nav-collapse collapse',
						'container_id'		=> 'navclose',
						'menu_class'		=> 'nav nav-pills',
						'fallback_cb'		=> 'wp_page_menu',
						'walker' 			=> new wp_bootstrap_navwalker()
					));
				?>
			</div>

			<div class="nav_flex_button">
				<?php if( get_field('featured_button', 'option')){ ?>
					<a<?php if (get_field('page_or_form', 'option') == 'link_to_page'){ ?> href="<?php the_field('featured_button_link', 'option'); ?>"<?php } ?><?php if (get_field('page_or_form', 'option') == 'link_to_form'){ ?> href="#popup_form"<?php } ?><?php if (get_field('page_or_form', 'option') == 'link_to_outside'){ ?> href="<?php the_field('featured_button_outside', 'option'); ?>" target="_blank"<?php } ?> class="btn_featured popup_form <?php the_field('page_or_form', 'option'); ?>"<?php if( get_field('nav_bottom_border', 'option') && get_field('nav_bottom_border_height', 'option')){ ?> style="border-width: <?php the_field('nav_bottom_border_height', 'option'); ?>px"<?php } ?>>
						<?php the_field('featured_button_text', 'option'); ?>
					</a>
				<?php } ?>
			</div>
		</div>
	</div><!-- row-fluid -->
</div><!-- container -->
</div><!-- navigation -->