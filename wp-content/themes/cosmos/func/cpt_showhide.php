<?php

/*-----------------------------------------------------------------------------------
	Custom Post Type - Show / Hide from Editor Role
-----------------------------------------------------------------------------------*/

function hide_cpt_from_editor() {

	// if( !current_user_can( 'administrator' ) ):

	$current_user = wp_get_current_user();
	if ( !('crankit' == $current_user->user_login)) :

		if ( !get_field('catalogues_cpt', 'option')):
			remove_menu_page( 'edit.php?post_type=catalogue' );
		endif;
		if ( !get_field('services_cpt', 'option')):
			remove_menu_page( 'edit.php?post_type=service' );
		endif;
		if ( !get_field('testimonials_cpt', 'option')):
			remove_menu_page( 'edit.php?post_type=testimonial' );
		endif;
		if ( !get_field('galleries_cpt', 'option')):
			remove_menu_page( 'edit.php?post_type=gallery' );
		endif;
		if ( !get_field('staff_cpt', 'option')):
			remove_menu_page( 'edit.php?post_type=staff' );
		endif;

		remove_menu_page( 'plugins.php' );

		remove_submenu_page( 'themes.php', 'themes.php' );
		remove_submenu_page( 'themes.php', 'nav-menus.php' );
		global $submenu;
		unset($submenu['themes.php'][6]); // remove customize link

		remove_menu_page( 'edit.php?post_type=acf-field-group' );
		remove_menu_page( 'itsec' );
		remove_menu_page( 'wpseo_dashboard' );
		remove_menu_page( 'cptui_manage_post_types' );

		remove_menu_page( 'options-general.php' );

	endif;

}
add_action( 'admin_menu', 'hide_cpt_from_editor' );