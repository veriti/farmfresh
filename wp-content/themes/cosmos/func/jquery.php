<?php

/*-----------------------------------------------------------------------------------
	jQuery Functions
-----------------------------------------------------------------------------------*/

// Add the List Item Icon to any <ul> with a class of .list_icons
function jquery_functions() {

	$list_item_icon = get_field('list_item_icon','option');
	//$bg_strike_image = get_field('body_background_pattern','option');

	echo '<script type="text/javascript">';
	echo 'jQuery(function ($) {';
	echo '$("ul.list_icons li").prepend("<i class=\'fa ' . $list_item_icon . '\'></i>");';
	//echo '$(".bg_strike").wrapInner( "<span style=\'background-image: url(' . $bg_strike_image . ');\'></span>" );';


	$banner_speed = get_field('banner_speed','option');
	echo "
		// Banner Slider
		$('#banner_slider').cycle({
			fx				: 'fade', 
			timeout			: " . $banner_speed . ",
			slides			: '>a,>img,>div',
			swipe			: true,
			prev			: '#btn_prev',
			next			: '#btn_next',
			pager			: '#banner_pager',
			pagerTemplate	: '<span></span>',
			autoHeight	: 'calc'
		});
	";




	echo '});';
	echo '</script>';
}
add_action( 'wp_footer', 'jquery_functions' );