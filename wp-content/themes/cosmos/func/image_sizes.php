<?php

/*-----------------------------------------------------------------------------------
	Custom Image Sizes
-----------------------------------------------------------------------------------*/

if ( function_exists( 'add_image_size' ) ) {}
	add_image_size( 'banner_image', 1200, 360, true ); //(cropped)
	add_image_size( 'cpt_banner_image', 750, 275, true ); //(cropped)
	//add_image_size( 'banner_image_hq', 2000, 600, true ); //(cropped)
	//add_image_size( 'featured_image_full_narrow', 1170, 250, true ); //(cropped)
	//add_image_size( 'featured_image_full', 1170, 350, true ); //(cropped)
	//add_image_size( 'featured_image_main_narrow', 770, 250, true ); //(cropped)
	//add_image_size( 'featured_image_main', 770, 350, true ); //(cropped)
	//add_image_size( 'gallery_image', 300, 200, true ); //(cropped)
	//add_image_size( 'boxlink_image', 370, 180, true ); //(cropped)
	add_image_size( 'ratio2-1', 500, 250, true ); //(cropped)
	add_image_size( 'ratio3-2', 500, 333, true ); //(cropped)