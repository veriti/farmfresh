<?php

/*-----------------------------------------------------------------------------------
	Activate Widgets Area(s)
-----------------------------------------------------------------------------------*/

function slx_register_sidebars() {
	/*register_sidebar(array(				// Start a series of sidebars to register
		'id' => 'footer', 					// Make an ID
		'name' => 'Footer',				// Name it
		'description' => 'Content for the right side Footer Block', // Dumb description for the admin side
		'before_widget' => '<div>',	// What to display before each widget
		'after_widget' => '</div>',	// What to display following each widget
		'before_title' => '<h3>',	// What to display before each widget's title
		'after_title' => '</h3>',		// What to display following each widget's title
		'empty_title'=> '',					// What to display in the case of no title defined for a widget
	));*/
	/*register_sidebar(array(				// Start a series of sidebars to register
		'id' => 'blog', 					// Make an ID
		'name' => 'Blog',				// Name it
		'description' => 'Content for the Blog sidebar', // Dumb description for the admin side
		'before_widget' => '<div class="sidebar_block">',	// What to display before each widget
		'after_widget' => '</div></div>',	// What to display following each widget
		'before_title' => '<h2>',	// What to display before each widget's title
		'after_title' => '</h2><div class="sidebar_block_inner">',		// What to display following each widget's title
		'empty_title'=> '',					// What to display in the case of no title defined for a widget
	));*/
}
add_action( 'widgets_init', 'slx_register_sidebars' );