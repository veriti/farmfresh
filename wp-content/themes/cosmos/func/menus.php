<?php

/*-----------------------------------------------------------------------------------
	Register Menus
-----------------------------------------------------------------------------------*/

register_nav_menus( 
	array(
		'primary'	=>	__( 'Primary Menu', 'slx' ), // Register the Primary menu
		'secondary'	=>	__( 'Secondary Menu', 'slx' ), // Register the Primary menu
	)
);