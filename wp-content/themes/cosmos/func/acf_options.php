<?php

/*-----------------------------------------------------------------------------------
	ACF Options Pages
-----------------------------------------------------------------------------------*/

if( function_exists('acf_add_options_page') ) {

	acf_add_options_page(array(
		'page_title' 	=> 'Website Settings',
		'menu_title'	=> 'Website Settings',
		'menu_slug' 	=> 'website-settings',
		'capability'	=> 'edit_posts'
		//'redirect'		=> false
	));
	
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Default Range',
		'menu_title'	=> 'Default Range',
		'parent_slug'	=> 'edit.php?post_type=deliveryexception'
	));

	acf_add_options_sub_page(array(
		'page_title' 	=> 'Site Details',
		'menu_title'	=> 'Site Details',
		'parent_slug'	=> 'website-settings'
	));


	// If current User = crankit
	$current_user = wp_get_current_user();
	if ( 'crankit' == $current_user->user_login ) {

		/*acf_add_options_sub_page(array(
			'page_title' 	=> 'Admin Site Details',
			'menu_title'	=> 'Admin Site Details',
			'parent_slug'	=> 'website-settings'
		));*/

		acf_add_options_sub_page(array(
			'page_title' 	=> 'Branding Colour Settings',
			'menu_title'	=> 'Branding Colours',
			'parent_slug'	=> 'website-settings'
		));

		acf_add_options_sub_page(array(
			'page_title' 	=> 'Background Settings',
			'menu_title'	=> 'Backgrounds',
			'parent_slug'	=> 'website-settings'
		));

		acf_add_options_sub_page(array(
			'page_title' 	=> 'Header Settings',
			'menu_title'	=> 'Header',
			'parent_slug'	=> 'website-settings'
		));

		acf_add_options_sub_page(array(
			'page_title' 	=> 'Navigation Settings',
			'menu_title'	=> 'Navigation',
			'parent_slug'	=> 'website-settings'
		));

		acf_add_options_sub_page(array(
			'page_title' 	=> 'Layout Settings',
			'menu_title'	=> 'Layout',
			'parent_slug'	=> 'website-settings'
		));

		acf_add_options_sub_page(array(
			'page_title' 	=> 'Banner Settings',
			'menu_title'	=> 'Banner',
			'parent_slug'	=> 'website-settings'
		));

		acf_add_options_sub_page(array(
			'page_title' 	=> 'Tracking Codes',
			'menu_title'	=> 'Tracking Codes',
			'parent_slug'	=> 'website-settings'
		));

		acf_add_options_sub_page(array(
			'page_title' 	=> 'CPT Visibility',
			'menu_title'	=> 'CPT Visibility',
			'parent_slug'	=> 'website-settings'
		));

	}

	// If current theme = Neptune 
	if ( wp_get_theme() == 'Neptune' ) {
		acf_add_options_sub_page(array(
			'page_title' 	=> 'Above Footer Block',
			'menu_title'	=> 'Above Footer',
			'parent_slug'	=> 'website-settings'
		));
	}

	// If current UserID = 2
	/*$user_ID = get_current_user_id();
	if ( $user_ID == '2' ) {
		acf_add_options_sub_page(array(
			'page_title' 	=> 'Testing2',
			'menu_title'	=> 'Testing2',
			'parent_slug'	=> 'website-settings'
		));
	}*/

}