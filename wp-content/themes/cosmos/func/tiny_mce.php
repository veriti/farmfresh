<?php

/*-----------------------------------------------------------------------------------
	TinyMCE
-----------------------------------------------------------------------------------*/

// Add Custome Styles to Editor
function my_theme_add_editor_styles() {
	add_editor_style( get_template_directory_uri() . '/styles/editor.css' );
}
add_action( 'admin_init', 'my_theme_add_editor_styles' );

// Add new Font Weight styles to the TinyMCE "formats" menu dropdown
if ( ! function_exists( 'wpex_styles_dropdown' ) ) {
	function wpex_styles_dropdown( $settings ) {
		// Create array of new styles
		$new_styles = array(
			array(
				'title'	=> __( 'Content Formatting', 'wpex' ),
				'items'	=> array(
					array(
						'title'		=> __('Side Block Right','wpex'),
						'block'		=> 'div',
						'classes'	=> 'side_content_block side_content_block_right',
						'wrapper'	=> 'true'
					),
					array(
						'title'		=> __('Side Block Left','wpex'),
						'block'		=> 'div',
						'classes'	=> 'side_content_block side_content_block_left',
						'wrapper'	=> 'true'
					),
					array(
						'title'		=> __('Background Strike','wpex'),
						'selector'	=> 'h1, h2, h3, h4, h5, h6',
						'classes'	=> 'bg_strike'
					),
					array(
						'title'		=> __('List Icons','wpex'),
						'selector'	=> 'ul',
						'classes'	=> 'list_icons'
					),
					array(
						'title'		=> __('Show Hide Block','wpex'),
						'block'		=> 'div',
						'classes'	=> 'showhide_block',
						'wrapper'	=> 'true'
					),
					array(
						'title'		=> __('Margin Bottom 0','wpex'),
						'selector'	=> '*',
						'classes'	=> 'slx_mb_0'
					)
				),
			),
			array(
				'title'	=> __( 'Font', 'wpex' ),
				'items'	=> array(
					array(
						'title'		=> __('Roboto','wpex'),
						'inline'	=> 'span',
						'classes'	=> 'f_roboto'
					),
					array(
						'title'		=> __('Roboto Condensed','wpex'),
						'inline'	=> 'span',
						'classes'	=> 'f_robotoC'
					),
					array(
						'title'		=> __('Roboto Slab','wpex'),
						'inline'	=> 'span',
						'classes'	=> 'f_robotoS'
					)
				),
			),
			array(
				'title'	=> __( 'Font Sizes', 'wpex' ),
				'items'	=> array(
					array(
						'title'		=> __('Font Size 14','wpex'),
						'inline'	=> 'span',
						'classes'	=> 'fs_14',
						'styles' => array(
							'fontSize'    => '14px'
						)
					),
					array(
						'title'		=> __('Font Size 16','wpex'),
						'inline'	=> 'span',
						'classes'	=> 'fs_16',
						'styles' => array(
							'fontSize'    => '16px'
						)
					),
					array(
						'title'		=> __('Font Size 18','wpex'),
						'inline'	=> 'span',
						'classes'	=> 'fs_18',
						'styles' => array(
							'fontSize'    => '18px'
						)
					),
					array(
						'title'		=> __('Font Size 20','wpex'),
						'inline'	=> 'span',
						'classes'	=> 'fs_20',
						'styles' => array(
							'fontSize'    => '20px'
						)
					),
					array(
						'title'		=> __('Font Size 22','wpex'),
						'inline'	=> 'span',
						'classes'	=> 'fs_22',
						'styles' => array(
							'fontSize'    => '22px'
						)
					),
					array(
						'title'		=> __('Font Size 24','wpex'),
						'inline'	=> 'span',
						'classes'	=> 'fs_24',
						'styles' => array(
							'fontSize'    => '24px'
						)
					),
					array(
						'title'		=> __('Font Size 28','wpex'),
						'inline'	=> 'span',
						'classes'	=> 'fs_28',
						'styles' => array(
							'fontSize'    => '28px'
						)
					),
					array(
						'title'		=> __('Font Size 30','wpex'),
						'inline'	=> 'span',
						'classes'	=> 'fs_30',
						'styles' => array(
							'fontSize'    => '30px'
						)
					),
					array(
						'title'		=> __('Font Size 32','wpex'),
						'inline'	=> 'span',
						'classes'	=> 'fs_32',
						'styles' => array(
							'fontSize'    => '32px'
						)
					),
					array(
						'title'		=> __('Font Size 36','wpex'),
						'inline'	=> 'span',
						'classes'	=> 'fs_36',
						'styles' => array(
							'fontSize'    => '36px'
						)
					),
					array(
						'title'		=> __('Font Size 40','wpex'),
						'inline'	=> 'span',
						'classes'	=> 'fs_40',
						'styles' => array(
							'fontSize'    => '40px'
						)
					),
					array(
						'title'		=> __('Font Size 44','wpex'),
						'inline'	=> 'span',
						'classes'	=> 'fs_44',
						'styles' => array(
							'fontSize'    => '44px'
						)
					),
				),
			),
			array(
				'title'	=> __( 'Font Weights', 'wpex' ),
				'items'	=> array(
					array(
						'title'		=> __('Thin','wpex'),
						'inline'	=> 'span',
						'classes'	=> 'fw_100',
						'styles' => array(
							'fontWeight'    => '100'
						)
					),
					array(
						'title'		=> __('Light','wpex'),
						'inline'	=> 'span',
						'classes'	=> 'fw_300',
						'styles' => array(
							'fontWeight'    => '300'
						)
					),
					array(
						'title'		=> __('Normal','wpex'),
						'inline'	=> 'span',
						'classes'	=> 'fw_400',
						'styles' => array(
							'fontWeight'    => '400'
						)
					),
					array(
						'title'		=> __('Medium','wpex'),
						'inline'	=> 'span',
						'classes'	=> 'fw_500',
						'styles' => array(
							'fontWeight'    => '500'
						)
					),
					array(
						'title'		=> __('Semibold','wpex'),
						'inline'	=> 'span',
						'classes'	=> 'fw_600',
						'styles' => array(
							'fontWeight'    => '600'
						)
					),
					array(
						'title'		=> __('Bold','wpex'),
						'inline'	=> 'span',
						'classes'	=> 'fw_700',
						'styles' => array(
							'fontWeight'    => '700'
						)
					),
					array(
						'title'		=> __('Extra Bold','wpex'),
						'inline'	=> 'span',
						'classes'	=> 'fw_800',
						'styles' => array(
							'fontWeight'    => '800'
						)
					),
					array(
						'title'		=> __('Ultra Bold','wpex'),
						'inline'	=> 'span',
						'classes'	=> 'fw_900',
						'styles' => array(
							'fontWeight'    => '900'
						)
					),
				),
			),
			array(
				'title'	=> __( 'Font Colours', 'wpex' ),
				'items'	=> array(
					array(
						'title'		=> __('White','wpex'),
						'inline'	=> 'span',
						'classes'	=> 'fc_white'
					),
					array(
						'title'		=> __('Light Grey','wpex'),
						'inline'	=> 'span',
						'classes'	=> 'fc_greyL'
					),
					array(
						'title'		=> __('Medium Grey','wpex'),
						'inline'	=> 'span',
						'classes'	=> 'fc_greyM'
					),
					array(
						'title'		=> __('Dark Grey','wpex'),
						'inline'	=> 'span',
						'classes'	=> 'fc_greyD'
					),
					array(
						'title'		=> __('Black','wpex'),
						'inline'	=> 'span',
						'classes'	=> 'fc_black'
					),
					array(
						'title'		=> __('Primary Colour','wpex'),
						'inline'	=> 'span',
						'classes'	=> 'fc_primary'
					),
					array(
						'title'		=> __('Secondary Colour','wpex'),
						'inline'	=> 'span',
						'classes'	=> 'fc_secondary'
					),
					array(
						'title'		=> __('Tertiary Colour','wpex'),
						'inline'	=> 'span',
						'classes'	=> 'fc_tertiary'
					),
				),
			),
			array(
				'title'	=> __( 'Buttons', 'wpex' ),
				'items'	=> array(
					array(
						'title'		=> __('Solid Button','wpex'),
						'selector'	=> 'a',
						'classes'	=> 'btn_solid'
					),
					array(
						'title'		=> __('Outline Button','wpex'),
						'selector'	=> 'a',
						'classes'	=> 'btn_outline'
					),
					array(
						'title'		=> __('Primary Button','wpex'),
						'selector'	=> 'a',
						'classes'	=> 'btn_primary'
					),
					array(
						'title'		=> __('Secondary Button','wpex'),
						'selector'	=> 'a',
						'classes'	=> 'btn_secondary'
					),
					array(
						'title'		=> __('Tertiary Button','wpex'),
						'selector'	=> 'a',
						'classes'	=> 'btn_tertiary'
					),
					array(
						'title'		=> __('Full Width Button','wpex'),
						'selector'	=> 'a',
						'classes'	=> 'btn_full'
					),
				),
			)
		);
		// Merge old & new styles
		$settings['style_formats_merge'] = true;
		// Add new styles
		$settings['style_formats'] = json_encode( $new_styles );
		// Return New Settings
		return $settings;
	}
}
add_filter( 'tiny_mce_before_init', 'wpex_styles_dropdown' );