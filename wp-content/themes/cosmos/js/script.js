//$(function() {
jQuery(function ($) {


	$(".popup_form").fancybox({
		maxWidth		: 800,
		maxHeight		: 600,
		fitToView		: false,
		width			: '70%',
		height			: '70%',
		autoSize		: false,
		closeClick		: false,
		openEffect		: 'none',
		closeEffect		: 'none'
	});
	
	// Slider Controls: Hide controls if only one slide
	var n = $('#banner_slider').children().length;
	if (n == 1) {
		$('.btn_slider').hide();
		$('#banner_pager').hide();
	}

	// Matchheight
	$('.box_link').matchHeight();

	// Make Contact Form 7 Behave
	$(".wpcf7").addClass("formHolder");
	//$("#footer_top .wpcf7-form-control-wrap input, #footer_top .wpcf7-form-control-wrap textarea").addClass("haslabel");
	//$(".form_row_inline .wpcf7-form-control-wrap").addClass("col-sm-9");
	$(".wpcf7-submit").addClass("btn_submit");

	// Add btn class to next/prev blog buttons
	$(".past-page a").addClass("btn_1");
	$(".next-page a").addClass("btn_1");

	// Banner Slider
	/*$('#banner_slider').cycle({
		fx			: 'fade', 
		timeout		: 4000,
		slides		: '>a,>img,>div',
		swipe		: true,
		autoHeight	: 'calc'
	});*/

	// Testimonial Slider
	$('.af_testimonials').cycle({
		fx			: 'fade', 
		timeout		: 4000,
		slides		: '>a,>img,>div',
		swipe		: true,
		autoHeight	: 'calc'
	});

	// Show/Hide Block
	$(".showhide_block").wrapInner("<div class='showhide_inner'></div>").prepend("<p><a class='showhide_link'>Read More...</a></p>");
	$( ".showhide_link" ).click(function() {
		if($('.showhide_inner:visible').length){
			$(this).text('Read More...');
			$('.showhide_inner').hide();
		} else {
			$(this).text('Read Less...');
			$('.showhide_inner').show();
		}
	});
	
	// Wrap Inner a <span> on any <h#> tag with class of bg_strike
	$( ".bg_strike" ).wrapInner( "<span></span>" );



	/*$('.nav-tabs a').click(function (e) {
		  e.preventDefault()
		  $(this).tab('show')
		})*/

 	// Add active class to first tab if no other tabs hasClass active
	if ( !$(".tabs .nav-tabs li:not(:first-child)").hasClass("active") ) {
		$(".tabs .nav-tabs li:first-child").addClass("active");
		$(".tabs .tab-content div:first-child").addClass("in active");	
	}

	// Add class="active" to the first tab on Product pages
	$(".tab_holder ul li:first-child").addClass("active");
	$(".tab_holder .tab-pane:first-child").addClass("active");

	// Put Label into Input
	$('input, textarea').each(function(){
		if($(this).hasClass('haslabel')){
			if($(this).attr('value') == '') {
				$(this).attr('value',$(this).parent().parent().find('label').text());
			}
		}else{}
	});
	
	$('input, textarea').focus(function(){
		if($(this).hasClass('haslabel')){
			if($(this).attr('value') == $(this).parent().parent().find('label').text()) {
				$(this).attr('value','');
			}
		}else{}
	});
	
	$('input, textarea').blur(function(){
		if($(this).hasClass('haslabel')){
			if($(this).attr('value') == '') {
				$(this).attr('value',$(this).parent().parent().find('label').text());
			}
		}else{}
	});




	// Navigation Slide In
	$('.sidenav_icon').click(function() {
		$('#navigation').addClass('slide-in');
		$('html').css("overflow", "hidden");
		$('#overlay').show();
		return false;
	});

	// Navigation Slide Out
	$('#overlay, .sidenav_close i').click(function() {
		$('#navigation').removeClass('slide-in');
		$('html').css("overflow", "auto");
		$('#overlay').hide();
		return false;
	});


	$('.nav-collapse li.menu-item-has-children > a').append("<i class='fa fa-chevron-right'></i>");
	$('.nav-collapse li.menu-item-has-children > a > i').click(function (e) {
		e.preventDefault();
		e.stopPropagation();
		var $parentli = $(this).closest('li');
		$parentli.find('> ul').stop().slideToggle();
		$parentli.toggleClass( "menu-item-open" );
		$(this).toggleClass('fa-chevron-right fa-chevron-down');
	});


});