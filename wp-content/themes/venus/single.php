<?php /* The template for Post Listing */
get_header();
?>

<div id="content">
<div class="container">
	<div id="mainContent" class="col-sm-9 pull-right">
		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
			<article class="post">
				<?php
					// set default post-banner background
					$post_banner = get_stylesheet_directory_uri() . '/images/post-banner.jpg';

					if (has_post_thumbnail( $post->ID ) ) {
						$post_banner = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
						$post_banner = $post_banner[0];
					}
				?>
				<div class="post-banner post-banner--full" style="background-image: url('<?php echo $post_banner; ?>')"></div>

				<div class="post-entry">

					<h3 class="post-title"><?php the_title(); ?></h3>

					<p><i class="fa fa-calendar"></i> <?php the_time('l, F jS, Y'); ?> | <?php the_author(); ?></p>
					<?php the_content(); ?>

				</div>

			</article>

			<nav class="nav nav-post-link">
				<?php previous_post_link('%link', '&lt; Previous entry') ?> | <?php next_post_link('%link', 'Next entry &gt;') ?>
			</nav>

		<?php endwhile; else : ?>
			<article class="post error">
				<h1 class="404">Nothing posted yet</h1>
			</article>
		<?php endif; ?>
	</div>
	<?php get_sidebar('posts'); ?>
</div><!-- container -->
</div><!-- content -->

<?php get_footer(); ?>
