<div id="navigation" class="<?php if( get_field('nav_bottom_border', 'option')){ ?> nav_bottom_border<?php } ?><?php if( get_field('navigation_font_style', 'option')){ ?> <?php the_field('navigation_font_style', 'option'); ?><?php } ?>"<?php if( get_field('nav_bottom_border', 'option') && get_field('nav_bottom_border_height', 'option')){ ?> style="border-width: <?php the_field('nav_bottom_border_height', 'option'); ?>px"<?php } ?>>
<div class="container">
	<div class="row">

		<div class="col-sm-12 nav_flex <?php the_field('navigation_font', 'option'); ?>">
			<!-- <a class="btn_menu collapsed bc_primary_dark" data-target="#navclose" data-toggle="collapse">Menu</a> -->
			<div class="sidenav_close"><i class="fa fa-times"></i></div>

			<div class="nav_flex_menu <?php the_field('nav_menu_width', 'option'); ?>">
				<?php
					wp_nav_menu( array(
						'menu'				=> 'primary',
						'theme_location'	=> 'primary',
						'depth'				=> 6,
						'container'			=> 'div',
						'container_class'	=> 'nav-collapse collapse',
						'container_id'		=> 'navclose',
						'menu_class'		=> 'nav nav-pills',
						'fallback_cb'		=> 'wp_page_menu',
						'walker' 			=> new wp_bootstrap_navwalker()
					));
				?>
			</div>
		</div>
	</div><!-- row-fluid -->
</div><!-- container -->
</div><!-- navigation -->
