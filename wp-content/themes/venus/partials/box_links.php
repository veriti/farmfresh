<?php if (get_field('box_link_title', 'option')){ ?>
	<h2><?php the_field('box_link_title', 'option'); ?></h2>
<?php } ?>

<?php if (get_field('box_link_type', 'option') == 'box_link_type_select') { ?>

	<?php
		$post_objects = get_field('box_link_select', 'option');
		if( $post_objects ):
		foreach( $post_objects as $post):
		setup_postdata($post);
	?>
		<div class="custom_post_item custom_post_item2">
			<div class="row">
				<div class="col-lg-3 col-md-4 col-sm-4">
					<div class="featured_image">
						<?php if (get_field('featured_image')){ ?>
							<?php $image = wp_get_attachment_image_src(get_field('featured_image'), 'ratio3-2'); ?>
							<a href="<?php echo get_permalink(); ?>">
								<img src="<?php echo $image[0]; ?>" alt="<?php the_title(); ?>" />
							</a>
						<?php } else { ?>
							<?php $image = wp_get_attachment_image_src(get_field('backup_image','option'), 'ratio3-2'); ?>
							<a href="<?php echo get_permalink(); ?>">
								<img src="<?php echo $image[0]; ?>" alt="<?php the_title(); ?>" />
							</a>
						<?php } ?>
					</div>
				</div>

				<div class="col-lg-9 col-md-8 col-sm-8 mobile_clear">
					<h4 class="title slx_mb_0">
						<a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a>
					</h4>
					<?php if (get_field('short_description')){ ?>
						<?php the_field('short_description'); ?>
					<?php } else { ?>
						<?php
							$content = get_the_content();
							$trimmed_content = wp_trim_words( $content, 25 );
							echo $trimmed_content;
						?>
					<?php } ?>
				</div>
			</div>
		</div>
	<?php
		endforeach;
		wp_reset_postdata();
		endif;
	?>

<?php } else if (get_field('box_link_type', 'option') == 'box_link_type_create') { ?>

	<?php if(get_field('box_link_create','option')): ?>
		<?php while(has_sub_field('box_link_create','option')): ?>

			<div class="custom_post_item custom_post_item2">
				<div class="row">
					<div class="col-lg-3 col-md-4 col-sm-4">
						<div class="featured_image">
							<?php if (get_sub_field('page_link','option')) { ?>
								<a href="<?php the_sub_field('page_link','option'); ?>"<?php if (get_sub_field('new_tab','option')) { ?> target="_blank"<?php } ?>>
							<?php } else if (get_sub_field('page_url','option')) { ?>
								<a href="<?php the_sub_field('page_url','option'); ?>"<?php if (get_sub_field('new_tab','option')) { ?> target="_blank"<?php } ?>>
							<?php } else { ?>
								<span>
							<?php } ?>
								<?php if (get_sub_field('image','option')){ ?>
									<?php $image = wp_get_attachment_image_src(get_sub_field('image','option'), 'ratio3-2'); ?>
									<img src="<?php echo $image[0]; ?>" alt="<?php the_title(); ?>" />
								<?php } else { ?>
									<?php $image = wp_get_attachment_image_src(get_field('backup_image','option'), 'ratio3-2'); ?>
									<img src="<?php echo $image[0]; ?>" alt="<?php the_title(); ?>" />
								<?php } ?>
							<?php if ( get_sub_field('page_link','option') || get_sub_field('page_url','option')){ ?>
								</a>
							<?php } else { ?>
								</span>
							<?php } ?>
						</div>
					</div>

					<div class="col-lg-9 col-md-8 col-sm-8 mobile_clear">
						<?php if (get_sub_field('title','option')){ ?>
							<h4 class="title slx_mb_0">
								<?php if (get_sub_field('page_link','option')) { ?>
									<a href="<?php the_sub_field('page_link','option'); ?>"<?php if (get_sub_field('new_tab','option')) { ?> target="_blank"<?php } ?>>
								<?php } else if (get_sub_field('page_url','option')) { ?>
									<a href="<?php the_sub_field('page_url','option'); ?>"<?php if (get_sub_field('new_tab','option')) { ?> target="_blank"<?php } ?>>
								<?php } else { ?>
									<span>
								<?php } ?>
									<?php the_sub_field('title','option'); ?>
								<?php if ( get_sub_field('page_link','option') || get_sub_field('page_url','option')){ ?>
									</a>
								<?php } else { ?>
									</span>
								<?php } ?>
							</h4>
						<?php } ?>

						<?php if (get_sub_field('box_link_blurb')){ ?>
							<p><?php the_sub_field('box_link_blurb'); ?></p>
						<?php } ?>
					</div>
				</div>
			</div>

		<?php endwhile; ?>
	<?php endif; ?>

<?php } // box_link_type ?>