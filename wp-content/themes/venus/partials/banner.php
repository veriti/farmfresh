<?php

if(get_field('show_banner') == "Yes") {

	if(get_field('banner')) { // Show the current banner if it exists
		$post_id = $post->ID;
	} else { // Revert to home page banner if no other banners exist and show banner is set to yes
		$post_id = 2;
	}
}

if (get_field('banner',$post_id) && get_field('show_banner') == "Yes") {
?>

<div id="banner" class="<?php the_field('banner_height',$post_id); ?> <?php the_field('banner_background_type', 'option'); ?>"
<?php if(get_field('banner_background_type', 'option') == 'banner_background_type_colour'){ ?>
	style="background-color: <?php the_field('banner_background_colour', 'option'); ?>"
<?php } ?>
<?php if(get_field('banner_background_type', 'option') == 'banner_background_type_pattern'){ ?>
	style="background-image: url('<?php the_field('banner_background_pattern', 'option'); ?>');"
<?php } ?>
>
	<div id="banner_slider">
		<?php
			while(has_sub_field('banner',$post_id)):
			$image = wp_get_attachment_image_src(get_sub_field('banner_image',$post_id), 'full');
		?>
		<div class="banner_slide">
			<div class="row row_image">
				<div class="col-sm-12">
					<div class="banner_slide_image_holder">
						<span class="banner_slide_image" style="background-image: url('<?php echo $image[0]; ?>');"></span>
					</div>
				</div>
			</div>

			<?php if (get_sub_field('banner_content',$post_id)): ?>
			<div class="banner_slide_content_holder">
				<div class="container">
					<div class="row row_content">
						<div class="col-sm-12">
							<div class="banner_slide_content<?php if (get_sub_field('banner_link_text')){ ?> has_banner_slide_button<?php } ?>">
								<?php the_sub_field('banner_content',$post_id); ?>
								<?php if (get_sub_field('banner_link_text')): ?>
									<div class="banner_slide_button">
										<a class="btn btn-lg <?php the_sub_field('banner_link_style',$post_id); ?>" href="<?php the_sub_field('banner_link_url',$post_id); ?>">
											<i class="fa <?php the_sub_field('banner_link_icon',$post_id); ?>"></i>
											<span><?php the_sub_field('banner_link_text',$post_id); ?></span>
										</a>
									</div>
								<?php endif; ?>
							</div>
						</div>
					</div>
				</div>
			</div>
			<?php endif; //banner_content ?>
		</div>
		<?php endwhile; ?>
	</div>

	<?php if( get_field('banner_buttons', 'option') ){ ?>
		<div id="btn_prev" class="btn_slider"></div>
		<div id="btn_next" class="btn_slider"></div>
	<?php } ?>

	<?php if( get_field('banner_pager', 'option') ){ ?>
		<div id="banner_pager"></div>
	<?php } ?>
</div>
<?php } ?>
