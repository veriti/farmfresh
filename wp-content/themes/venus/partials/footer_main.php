<footer>
<div class="container">
	<div class="row">
		<h3>Join us on an ethical food adventure! <a href="<?php echo get_permalink( woocommerce_get_page_id('shop') ) ?>">Shop now</a></h3>
		<div class="col-sm-3 footer-contact">
			<h4>Get In Touch</h4>
			<dl>
				<dt><i class="fa fa-phone"></i></dt>
				<dd><a href="tel:0731622474">(07) 3162 2474</a></dd>
				<dt><i class="fa fa-fax"></i></dt>
				<dd>(07) 3162 3928</dd>
				<dt><i class="fa fa-home"></i></dt>
				<dd>5/9 Shoebury St Rocklea 4106<br><small>(Please note this is a warehouse not open to the public)</small></dd>
				<dt><i class="fa fa-facebook-official"></i></dt>
				<dd><a href="#">Latest Organic News</a></dd>
			</dl>
		</div>
		<div class="col-sm-3 footer-about">
			<h4>About Us</h4>
			<p>Put simply, we're on a mission to save the world, ad we're doing it one meal, one person, one family at a time, starting with you!</p>
			<p>
				<a href="#">
					<img src="<?php echo get_stylesheet_directory_uri() . '/images/thumb-footer-video.jpg' ?>" alt="Watch Farm Fresh in action">

				</a>
			</p>
		</div>
		<div class="col-sm-3 footer-newsletter">
			<h4>Newsletter</h4>
			<p>Would you like to receive bi-weekly reminder emails with ordering dead-lines, what's new, seasonal specials, and recipe ideas?</p>
			<?php echo do_shortcode('[contact-form-7 id="21886" title="Newsletter"]'); ?>
		</div>
		<div class="col-sm-3 footer-cta">
			<p>
				<a href="#">
					<img src="<?php echo get_stylesheet_directory_uri() . '/images/cta-freedelivery.png' ?>" alt="Free Delivery">
				</a>
			</p>
		</div>
	</div>
</div><!-- container -->
</footer>
