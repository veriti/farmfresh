<?php
/* This file will render the sidebar, as defined by the user in the admin area */
?>
<div class="sidebar col-sm-3 pull-left">
	<div class="row sidebar-category">
		<div class="sidebar_header">
			<ul>
				<li>
					<span>Categories</span>
					<?php
					wp_nav_menu( array(
						'container'      => false,
						'menu'           => 'sidebar',
						'theme_location' => 'sidebar',
						'depth'          => 6,
					) );
					?>
					<?php echo '<ul>' ?>
				</li>
			</ul>
		</div>

		<div class="sidebar_recent">
			<ul>
				<li><span>Recent Posts</span>
					<ul>
						<?php
						$recent_posts = wp_get_recent_posts( array(
							'numberposts' => 3
						) );
						foreach ( $recent_posts as $recent ) {
							echo '<li>';
							echo '<a class="recent-title" href="' . get_permalink( $recent["ID"] ) . '">' . $recent["post_title"] . '</a>';
							echo '<span class="recent-date">' . mysql2date( 'd M Y', $recent["post_date"] ) . '</span>';
							echo '</li>';
						}
						wp_reset_query();
						?>
					</ul>
				</li>
			</ul>
		</div>
	</div>

</div>
