<?php
/* Template Name: Postcode Search */
get_header();
?>

<div id="content">

	<div class="container">
		<div class="row">
			<div id="mainContent" class="col-sm-12">
				<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

					<?php if ( get_field( 'featured_image' ) && ! $post->post_content == "" ) { ?>

						<div class="row">
							<?php if ( get_field( 'featured_image_type' ) == 'featured_long' ) { ?>

								<div class="col-sm-12">
									<div class="featured_image">
										<?php $image = wp_get_attachment_image_src( get_field( 'featured_image' ), 'cpt_banner_image' ); ?>
										<?php $imageF = wp_get_attachment_image_src( get_field( 'featured_image' ), 'full' ); ?>
										<a href="<?php echo $imageF[0]; ?>" rel="gallery">
											<img src="<?php echo $image[0]; ?>"/>
										</a>
									</div>
								</div>
								<div class="col-sm-12">
									<?php the_content(); ?>
								</div>

							<?php } elseif ( get_field( 'featured_image_type' ) == 'featured_short' ) { ?>

								<div class="col-lg-4 col-md-6 col-sm-12">
									<div class="featured_image">
										<?php $image = wp_get_attachment_image_src( get_field( 'featured_image' ), 'full' ); ?>
										<a href="<?php echo $image[0]; ?>" rel="gallery">
											<img src="<?php echo $image[0]; ?>"/>
										</a>
									</div>
								</div>
								<div class="col-lg-8 col-md-6 col-sm-12">
									<?php the_content(); ?>
								</div>

							<?php } ?>
						</div>

					<?php } elseif ( get_field( 'featured_image' ) && $post->post_content == "" ) { ?>

						<div class="row">
							<div class="col-sm-12">
								<div class="featured_image">
									<?php $image = wp_get_attachment_image_src( get_field( 'featured_image' ), 'cpt_banner_image' ); ?>
									<?php $imageF = wp_get_attachment_image_src( get_field( 'featured_image' ), 'full' ); ?>
									<a href="<?php echo $imageF[0]; ?>" rel="gallery">
										<img src="<?php echo $image[0]; ?>"/>
									</a>
								</div>
							</div>
						</div>

					<?php } elseif ( ! $post->post_content == "" ) { ?>

						<div class="row">
							<div class="col-sm-12">
								<?php the_content(); ?>
							</div>
						</div>

					<?php } ?>

				<?php endwhile;
				else : ?>

					<article class="post error">
						<h1 class="404">No Pages have been posted yet</h1>
					</article>

				<?php endif; ?>
				<?

				if ( is_page( 'postcode-search' ) ):

					$postcode = esc_sql( $_GET['postcode'] );
					global $wpdb;

					$args = array(
						'post_type' => 'deliveryexception'
					);


					$postcodes = new WP_query( $args );

					if ( $postcodes->have_posts() ) :
						while ( $postcodes->have_posts() ) : $postcodes->the_post();

							if ( $postcode >= get_field( 'postcode_start' ) && $postcode <= get_field( 'postcode_end' ) ) {
								$locations[] = get_the_title();
							}
						endwhile;
					endif;

					$locations = array_unique( $locations );

					?>

					<?php
					$queried_object = get_queried_object();
					$page_slug = $queried_object->post_name;
					?>

					<div class="delivery">
						<div class="delivery-content">
							<div class="delivery-block">
								<h3>Check your delivery area</h3>
							</div>
							<div class="delivery-block">
								<form action="<?php echo $page_slug ?>" class="form-inline">
									<div class="form-group">
										<input type="text" class="form-control" name="postcode" title="postcode"
										       placeholder="Enter your postcode"/>
										<button type="submit" class="btn btn-default">Check</button>
									</div>
								</form>
							</div>
						</div>
					</div>

					<div class="data-table">
						<?php
						/* Query the Suburb name from Postcode */
						$geocode = $wpdb->get_results( "SELECT * FROM postcodes_geo WHERE postcode = '$postcode'" );

						foreach ( $geocode as $geo ):
							?>
							<div class="data-row">
								<div class="data-cell"><strong><?php echo $_GET['postcode']; ?></strong></div>
								<div class="data-cell"><strong><?php echo $geo->suburb; ?></strong></div>
								<div class="data-cell">
									<strong>Tuesday - Afternoon Delivery</strong><br/>*If ordered by Monday 8.30AM
								</div>
								<div class="data-cell"><strong>FREE*</strong></div>
							</div>
							<div class="data-row">
								<div class="data-cell">&nbsp;</div>
								<div class="data-cell">&nbsp;</div>
								<div class="data-cell">
									<strong>Tuesday - Afternoon Delivery</strong><br/>*If ordered by Monday 8:30AM
								</div>
								<div class="data-cell"><strong>FREE*</strong></div>
							</div>
						<? endforeach; ?>
					</div>
					<?
					if ( ! $geocode ):
						echo "<p>Sorry we do not deliver to that postcode. Please check back later.</p>";
					endif;
					?>
					</form>
				<?php endif; ?>

			</div>

		</div><!-- row -->
	</div><!-- container -->
</div><!-- content -->

<?php
if ( wp_get_theme() != 'Venus' ) {
	if ( get_field( 'services_box', 'option' ) ) {
		get_template_part( 'partials/box_links_holder' );
	}
}
?>

<?php get_footer(); ?>
