<?php
/* Load WordPress Environment */
include("../../../wp-config.php");

/* Only return orders within the date range*/
$start = mysql_real_escape_string($_GET['start']);
$finish = mysql_real_escape_string($_GET['finish']);
?>
<html>
   <head>
      <title>Product Report</title>
   </head>
   <style>
   body { 
   	font-size: 12px;
   	font-family: arial;
   }
   </style>
   <table border=1 cellpadding=4 cellspacing=0>
      <tr>
         <td bgcolor='aaaaaa'>&nbsp;</td>
         <td bgcolor='aaaaaa'><b>Product Name</b></td>
         <td bgcolor='aaaaaa'><b>Total Sold</b></td>
         <td bgcolor='aaaaaa'><b>Price Ex</b></td>
         <td bgcolor='aaaaaa'><b>Total Sale Value</b></td>
      </tr>

      <?php
         $args = array(
            'post_type' => 'shop_order',
            'post_status' => 'wc-processing',
            'posts_per_page' => -1,
            'date_query' => array(
                array(
                  'after'     => $start,
                  'before'     => $finish,
                )
              ),
            );
         $loop = new WP_Query( $args );
      
         while ( $loop->have_posts() ) : $loop->the_post();

         $details = new WC_Order(get_the_ID());
         $product = $details->get_items();

         foreach($product as $order_product_detail) {
            $products[$order_product_detail['product_id']]['qty'] = $products[$order_product_detail['product_id']]['qty'] + $order_product_detail['qty'];
            $products[$order_product_detail['product_id']]['line_total'] = $products[$order_product_detail['product_id']]['line_total'] + $order_product_detail['line_total'];
         }

         endwhile;

         foreach($products as $key => $qty) { 
      ?>
      <tr>
         <td bgcolor='aaaaaa'><font size='2'><?php echo $key; ?></font></td>
         <td><font size='2'><?php echo get_the_title($key);?></font></td>
         <td><font size='2'><?php echo $qty['qty']; ?></font></td>
         <td><font size='2'>$<?php echo number_format($qty['line_total'] / $qty['qty'],2); ?></font></td>
         <td><font size='2'>$<?php echo number_format($qty['qty'] * get_post_meta($key, '_price', true ),2); ?></font></td>
      </tr>
      <? } ?>
   </table>
</body>
</html>