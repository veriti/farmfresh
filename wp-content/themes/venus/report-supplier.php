<?php
/* Load WordPress Environment */
include("../../../wp-config.php");

$batchID = mysql_real_escape_string($_GET['batchID']);

/* Get the orders in the batch */
$orders = get_field('orders',$batchID);


/* Output Orders Loop */
foreach($orders as $order):

	/* Order Details */
	$details = new WC_Order($order);

	$product = $details->get_items(); 

    foreach($product as $order_product_detail):

		$supplier = wp_get_post_terms($order_product_detail['product_id'],'suppliers');
		$suppliers[$supplier[0]->term_id]['name'] = $supplier[0]->name;
		$suppliers[$supplier[0]->term_id]['products'][] = $order_product_detail['qty'].','.$order_product_detail['product_id'];

	endforeach;

endforeach;

?>
<html>
<head><meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
    <title>Supplier Orders List For Batch 2426</title>
</head>
  <body>
 <?php foreach($suppliers as $report): ?>
    <table border="0" cellpadding="0" cellspacing="0" style="width:170mm;">
      <tbody>
      <tr>
        <td>
          <b>Supplier: <?php echo $report['name']; ?></b><br>
          <hr width="100%">
        </td>
      </tr>
      <tr>
        <td>
          <table border="0" cellpadding="0" cellspacing="0" style="width:170mm;">
            <tbody><tr>
              <td style="width:40mm;"><em>Product Code</em></td>
              <td style="width:115mm;"><em>Product Name</em></td>
              <td style="width:15mm;" align="right"><em>Quantity</em></td>
            </tr>
            <tr>
              <td width="100%" colspan="3"><hr width="100%"></td>
            </tr>
            <?php 
           		foreach($report['products'] as $product): 
            	$product = explode(',',$product);
            	$quantity = $product[0];
            	$product_detail = new WC_Product($product[1]);
            ?>
            <tr>
              <td style="width:40mm;"><?php echo $product_detail->sku; ?></td>
              <td style="width:115mm;"><?php echo $product_detail->get_title(); ?></td>
              <td style="width:15mm;" align="right"><?php echo $quantity;?></td>
            </tr>
        	<?php endforeach; ?>
          </tbody></table>
        </td>
      </tr>
      <tr>
        <td><hr width="100%">
      </td></tr>
    </tbody></table>
    <br>
    <br>
<? endforeach; ?>
</body></html>