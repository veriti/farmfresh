<?php
/* Template Name: Home Page */
get_header();
?>

<div class="banner">
	<div class="hero container">
		<h1>We're Brisbane's award-winning organic home delivery service.</h1>
		<p><a href="<?php echo get_permalink( woocommerce_get_page_id( 'shop' ) ) ?>" class="btn btn-lg btn-primary">Shop
				Now</a></p>
	</div>
	<div class="awards container">
		<div class="award">
			<img src="<?php echo get_stylesheet_directory_uri() . '/images/award-1.png' ?>"
			     alt="WINNER: HONOURS AWARD 2015 - 10 years of carbon offsetting">
		</div>
		<div class="award">
			<img src="<?php echo get_stylesheet_directory_uri() . '/images/award-2.png' ?>"
			     alt="WINNER: HONOURS AWARD 2015 - 10 years of carbon offsetting">
		</div>
		<div class="award">
			<img src="<?php echo get_stylesheet_directory_uri() . '/images/award-3.png' ?>"
			     alt="WINNER: HONOURS AWARD 2015 - 10 years of carbon offsetting">
		</div>
		<div class="award">
			<img src="<?php echo get_stylesheet_directory_uri() . '/images/award-4.png' ?>"
			     alt="WINNER: HONOURS AWARD 2015 - 10 years of carbon offsetting">
		</div>
		<div class="award">
			<img src="<?php echo get_stylesheet_directory_uri() . '/images/award-5.png' ?>"
			     alt="WINNER: HONOURS AWARD 2015 - 10 years of carbon offsetting">
		</div>
		<div class="award">
			<img src="<?php echo get_stylesheet_directory_uri() . '/images/award-6.png' ?>"
			     alt="WINNER: HONOURS AWARD 2015 - 10 years of carbon offsetting">
		</div>
	</div>
</div>

<div id="content">
	<div id="mainContent">
		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
			<div class="container">
				<div class="col-xs-10 col-xs-offset-1">

					<?php the_content(); ?>
				</div>
			</div>
		<?php endwhile;
		else : ?>

			<article class="post error">
				<h1 class="404">Nothing posted yet</h1>
			</article>

		<?php endif; ?>

	</div>

	<div class="keypoints">
		<div class="container">
			<div class="col-sm-12">
				<div class="keypoint">
					<img src="<?php echo get_stylesheet_directory_uri() . '/images/icon-delivery.png' ?>"
					     alt="Delivery Icon">
					<h4>Delivery Twice Per Week</h4>
				</div>
				<div class="keypoint">
					<img src="<?php echo get_stylesheet_directory_uri() . '/images/icon-organic.png' ?>"
					     alt="Delivery Icon">
					<h4>Certified Organic</h4>
				</div>
				<div class="keypoint">
					<img src="<?php echo get_stylesheet_directory_uri() . '/images/icon-products.png' ?>"
					     alt="Delivery Icon">
					<h4>1000&amp;s of Products</h4>
				</div>
				<div class="keypoint">
					<img src="<?php echo get_stylesheet_directory_uri() . '/images/icon-quality.png' ?>"
					     alt="Delivery Icon">
					<h4>100% Quality Guaranteed</h4>
				</div>
				<div class="keypoint">
					<img src="<?php echo get_stylesheet_directory_uri() . '/images/icon-farmer.png' ?>"
					     alt="Delivery Icon">
					<h4>Supporting Local Farmers</h4>
				</div>
				<div class="keypoint">
					<img src="<?php echo get_stylesheet_directory_uri() . '/images/icon-co2.png' ?>"
					     alt="Delivery Icon">
					<h4>Carbon Offsetting</h4>
				</div>
			</div>
		</div>
	</div>

	<div class="delivery">
		<div class="container">
			<div class="delivery-content">
				<div class="delivery-block">
					<h3>Check your delivery area</h3>
				</div>
				<div class="delivery-block">
					<form action="/postcode-search" class="form-inline">
						<div class="form-group">
							<input type="text" class="form-control" name="postcode" title="postcode" placeholder="Enter your postcode" />
							<button type="submit" class="btn btn-default">Check</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>

	<div class="categories">
		<?php
		$homepageCats = array(
			"4060", // bakery
			"4055", // butcher
			"4063", // dairy
			"4062", // fruits & veg
			"4057", // grocery store
			"4061", // organic gardening
			"4058", // eco store
		);

		$args = array(
			"taxonomy"   => "product_cat",
			"hide_empty" => false,
			"include"    => $homepageCats,
			"orderby"    => "term_order",
		);

		$terms = get_terms( 'product_cat', $args );

		if ( $terms ) {
			echo '<div class="container">';
			echo '<ul class="category-group">';
			foreach ( $terms as $term ) {
				$thumbnail_id  = get_woocommerce_term_meta( $term->term_id, 'thumbnail_id', true );
				$thumbnail_url = wp_get_attachment_image_src( $thumbnail_id, 'full' );
				echo '<li class="category">';
				echo '<a href="' . esc_url( get_term_link( $term ) ) . '" class="' . $term->slug . '">';
				echo '<h4>';
				echo $term->name;
				echo '</h4>';
				echo '<img src="' . $thumbnail_url[0] . '" alt="">';
				echo '</a>';
				echo '</li>';
			}
			echo '</ul>';
			echo '</div>';
		}
		?>
	</div>

	<div class="farmers">
		<div class="container">
			<div class="farmers-content col-sm-7">
				<h3>Meet the farmers</h3>
				<hr class="rule rule--light">
				<p>Our home farm is located on Tamborine Mountain, with rich volcanic soils, abundant rain, clean
					mountain air and a mineral-rich underground aquifer. We grow blueberries, citrus, olives, speciality
					herbs, greens, and seasonal vegetables.</p>
				<p>Blueberry Hill Organic Farm is a haven for local wildlife, and a beautiful place to live and grow. Up
					before the sun, we feed the chooks, check farmer.</p>
				<p><a href="#" class="btn btn-primary btn-lg">Learn More</a></p>
			</div>
			<div class="farmers-image col-sm-5">
				<img src="<?php echo get_stylesheet_directory_uri() . '/images/image-farmers.png' ?>"
				     alt="Meet Our Farmers">
			</div>
		</div>
	</div>

	<div class="testimonials">
		<div class="fruits fruits-left"></div>
		<div class="fruits fruits-right"></div>
		<div class="container">
			<div class="col-sm-12 text-center">
				<h3>Our customers love us, and you will too!</h3>
				<hr class="rule">
			</div>
		</div>
		<div class="container">
			<?php
			$args         = array( 'post_type' => 'testimonials' );
			$testimonials = get_posts( $args );
			foreach ( $testimonials as $post ) : setup_postdata( $post ); ?>
				<div class="testimonial col-sm-6 col-md-4">
					<div class="rating">
						<?php // TODO: loop through ratings if available ?>
						<span class="rating-star"></span>
						<span class="rating-star"></span>
						<span class="rating-star"></span>
						<span class="rating-star"></span>
						<span class="rating-star"></span>
					</div>
					<blockquote>
						<?php the_content(); ?>
						<cite>
							<?php the_title(); ?>
						</cite>
					</blockquote>
				</div>
			<?php endforeach;
			wp_reset_postdata(); ?>
		</div>
	</div>

</div><!-- content -->
<?php get_footer(); ?>
