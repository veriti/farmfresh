<?php
/* This file will render the sidebar, as defined by the user in the admin area */
?>
<div class="sidebar col-sm-3 pull-left">
	<div class="row">
		<div class="sidebar-search">
			<form role="search" method="get" action="<?php echo home_url( '/' ) ?>" class="form-inline">
				<div class="input-group">
					<input type="text" class="form-control" name="s" placeholder="Search products..." value="<?php get_search_query() ?>"/>
					<span class="input-group-btn">
						<button type="submit" class="btn btn-search">SEARCH</button>
					</span>
				</div>
			</form>
		</div>

		<div class="sidebar-category">
			<?php
			$query_object = get_queried_object();

			function get_current_cat_title( $query_object ) {

				// match category
				if ( property_exists( $query_object, 'taxonomy' ) ) {
					$term_id   = ( $query_object->parent != 0 ) ? $query_object->parent : $query_object->term_id;
					$term_link = ( $term_id ) ? get_term_link( $term_id ) : null;

					// match top category
					if ( $query_object->parent == 0 ) {
						return '<a href="' . $term_link . '">' . $query_object->name . '</a>';
					}

					// match subcategory
					$term = get_term_by( 'id', $term_id, 'product_cat' );

					return '<a href="' . $term_link . '">' . $term->name . '</a>';

				}

				// nothing matched, return empty
				return null;

			}

			function get_current_cat_list( $query_object ) {
				$term_id = ( $query_object->parent != 0 ) ? $query_object->parent : $query_object->term_id;

				$args = array(
					'taxonomy'     => 'product_cat',
					'child_of'     => $term_id,
					'show_count'   => false,
					'pad_counts'   => false,
					'hierarchical' => true,
					'title_li'     => false,
					'echo'         => false,
				);

				if ( $query_object->term_id ) {
					return '<ul>' . wp_list_categories( $args ) . '</ul>';
				}

				return '';
			}


			if ( $query_object->post_type != 'page' ) {
				echo '<div class="sidebar_header">';
				echo '<ul>';
				echo '<li>';
				echo get_current_cat_title( $query_object );
				echo get_current_cat_list( $query_object );
				echo '</li>';
				echo '</ul>';
				echo '</div>';
			}
			?>

			<?php
			// Display the custom sidebar menu
			wp_nav_menu( array(
				'menu'            => 'sidebar',
				'theme_location'  => 'sidebar',
				'depth'           => 6,
				'container_class' => 'sidebar_menu'
			) );
			?>
		</div>
	</div>
</div>
