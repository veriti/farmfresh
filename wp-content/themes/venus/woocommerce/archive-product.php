<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woothemes.com/document/template-structure/
 * @author      WooThemes
 * @package     WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

get_header( 'shop' ); ?>

    <?php
        /**
         * woocommerce_before_main_content hook.
         *
         * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
         * @hooked woocommerce_breadcrumb - 20
         */
        do_action( 'woocommerce_before_main_content' );
    ?>

    <?php
        $queried_object = get_queried_object();
        $cat_banner_url = NULL;

        if ( $queried_object->parent != 0 ) {
            $term = get_term_by( 'id', $queried_object->parent, 'product_cat' );
            $cat_banner_url = get_field('cat_banner', $term);
        }

        if ( get_field( 'cat_banner', $queried_object) ) {
            $cat_banner_url = get_field( 'cat_banner', $queried_object);
        }
    ?>

    <div class="page-head" style="<?php echo ($cat_banner_url) ? 'background-image: url(' . $cat_banner_url . ')' : ''; ?>">
        <div class="page-head-content">
        <?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>

            <h1 class="page-title"><?php woocommerce_page_title(); ?></h1>

        <?php endif; ?>

        <?php
            /**
             * woocommerce_archive_description hook.
             *
             * @hooked woocommerce_taxonomy_archive_description - 10
             * @hooked woocommerce_product_archive_description - 10
             */
            do_action( 'woocommerce_archive_description' );
        ?>
        </div>
    </div>

    <div class="page-content col-sm-9 pull-right">
    <?php if ( have_posts() ) : ?>

        <?php
            /**
             * woocommerce_before_shop_loop hook.
             *
             * @hooked woocommerce_result_count - 20
             * @hooked woocommerce_catalog_ordering - 30
             */
            do_action( 'woocommerce_before_shop_loop' );
        ?>

        <?php $subCategories = get_term_children($queried_object->term_id, 'product_cat') ?>
        <?php if($queried_object->post_type == 'page'): $categories = get_terms('product_cat'); ?>
            <?php foreach($categories as $category): ?>
                <?php if($category->count && ($category->parent == 0)): ?>
                    <h4><?php echo $category->name ?></h4>
                    <?php echo do_shortcode('[product_category category="' . $category->slug . '"]'); ?>
                <?php endif; ?>
            <?php endforeach; ?>
        <?php elseif(count($subCategories)): ?>
            <?php foreach($subCategories as $subCategory): $subTerm = get_term_by( 'id', $subCategory, 'product_cat' ); ?>
                <?php if($subTerm->count): ?>
                    <h4><?php echo $subTerm->name ?></h4>
                    <?php echo do_shortcode('[product_category category="' . $subTerm->slug . '"]'); ?>
                <?php endif; ?>
            <?php endforeach; ?>
        <?php else: ?>
            <?php woocommerce_product_loop_start(); ?>
                <?php woocommerce_product_subcategories(); ?>
                <?php while ( have_posts() ) : the_post(); ?>
                    <?php wc_get_template_part( 'content', 'product' ); ?>
                <?php endwhile; // end of the loop. ?>
            <?php woocommerce_product_loop_end(); ?>
            <?php
                /**
                 * woocommerce_after_shop_loop hook.
                 *
                 * @hooked woocommerce_pagination - 10
                 */
                do_action( 'woocommerce_after_shop_loop' );
            ?>
        <?php endif; ?>

    <?php elseif ( ! woocommerce_product_subcategories( array( 'before' => woocommerce_product_loop_start( false ), 'after' => woocommerce_product_loop_end( false ) ) ) ) : ?>

        <?php wc_get_template( 'loop/no-products-found.php' ); ?>

    <?php endif; ?>

    <?php
        /**
         * woocommerce_after_main_content hook.
         *
         * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
         */
        do_action( 'woocommerce_after_main_content' );
    ?>

    <?php
        /**
         * woocommerce_sidebar hook.
         *
         * @hooked woocommerce_get_sidebar - 10
         */
        do_action( 'woocommerce_sidebar' );
    ?>

<?php get_footer( 'shop' ); ?>
