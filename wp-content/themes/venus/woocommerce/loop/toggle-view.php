<div class="products-view">
    <div class="products-view-notice">
        <p><?php echo get_option( 'woocommerce_demo_store_notice' ); ?></p>
    </div>
    <div class="products-view-toggle">
        <button type="button" class="btn" data-view="text"><span class="fa fa-align-justify"></span></button>
        <button type="button" class="btn" data-view="list"><span class="fa fa-list"></span></button>
        <button type="button" class="btn" data-view="grid"><span class="fa fa-th"></span></button>
    </div>
</div>
