<div class="cart-guarantee">
	<div class="woocommerce-message">
		<?php $message = "If you're not 100% satisfied with the quality of any of your produce, just let us know by email/phone/text (or at the checkout when you next order) and we'll happily refund or replace the offending item."; ?>
		<p><?php echo $message ?></p>
	</div>
</div>
