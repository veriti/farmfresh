/*global jQuery*/
(function ($, d) {
  'use strict';

  // Setup quantity increment / decrement
  function initQuantityField() {
    $('.quantity').each(function () {
      var $this = $(this);
      
      if ($this.is('.quantified')) {return;}

      // increment
      var $increment = $('<button/>', { type: 'button', text: '>', 'class': 'increment' })
        .on('click', function () {
          $input.get(0).value++;
        });

      $this.append($increment);

      // decrement
      var $decrement = $('<button/>', { type: 'button', text: '<', 'class': 'decrement' })
        .on('click', function () {
          if ($input.get(0).value <= 1) {return;}
          $input.get(0).value--;
        });

      $this.prepend($decrement);
      $this.addClass('quantified');

      // For some reason this needs to trigger
      var $input = $(this).find('input[type=number]')
        .trigger('change');

    });
  }

  function _loadThumbnails() {
    $('.product-thumbnail[data-image]').each(function () {
      var $this = $(this);

      if ($this.find('img').length) {return;}

      var $img = $('<img/>', {
        src: $this.data('image') || ''
      });

      $this.prepend($img);

    });

  }

  function _setView(view) {
    var $products = $('.products');

    if ($products.is('.related .products')) {
      $products.addClass('products--grid');
      return;
    }

    $products.removeClass('products--list products--grid');

    switch (view) {
      case 'list':
        $products.addClass('products--list');
        _loadThumbnails();
        break;
      case 'grid':
        $products.addClass('products--grid');
        _loadThumbnails();
        break;
    }
  }

  function initProductView(label) {
    var $toggleClass = '.products-view-toggle .btn';
    var store = JSON.parse(localStorage.getItem(label));

    // if store is empty just ignore
    if (!JSON.parse(localStorage.getItem(label))) {
      localStorage.setItem(label, JSON.stringify({ view: 'list' }));
      _setView('list');
    }

    // bind button
    $(document).on('click', $toggleClass, function () {
      store = JSON.parse(localStorage.getItem(label));
      var $button = $(this);

      store.view = $button.data('view');
      localStorage.setItem(label, JSON.stringify(store));
      _setView(store.view);
    });
    
    setTimeout(function() { _setView(store.view); }, 100);
  }
  
  function doBindings() {
    // Rebuild the incrementer on cart update
    $(document).on('updated_cart_totals', function (e) {
      // prevent calling multiple times
      if (d.cartLastUpdated && d.cartLastUpdated === e.timeStamp) {return;}
      j.cartLastUpdated = e.timeStamp;
      initQuantityField();
    });

    $(document).on('yith_infs_added_elem', function() {
      _loadThumbnails();
      initQuantityField();
    });
  }

  // On load
  $(function () {
    initQuantityField();
    initProductView('display_config');
    doBindings();
  });

})(jQuery, document);
