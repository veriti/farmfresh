<?php

// Enqueue Styles
function theme_enqueue_styles() {
	wp_enqueue_style( 'child-fonts-external', 'https://fonts.googleapis.com/css?family=Shadows+Into+Light+Two', false );
	wp_enqueue_style( 'slx-bootstrap-style', get_template_directory_uri() . '/styles/bootstrap.min.css' );
	wp_enqueue_style( 'child-styles', get_stylesheet_directory_uri() . '/less/style.less', false, '0.0.1', 'all' );
	wp_enqueue_script( 'child-scripts', get_stylesheet_directory_uri() . '/script.js', false, '0.0.1', true );
}

add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );

add_action( 'woocommerce_checkout_process', 'wc_minimum_order_amount' );
add_action( 'woocommerce_before_cart', 'wc_minimum_order_amount' );

function wc_minimum_order_amount() {
	// Set this variable to specify a minimum order value
	$minimum = 50;

	if ( WC()->cart->total < $minimum ) {

		if ( is_cart() ) {

			wc_print_notice(
				sprintf( 'You must have an order with a minimum of %s to place your order, your current order total is %s.',
					wc_price( $minimum ),
					wc_price( WC()->cart->total )
				), 'error'
			);

		} else {

			wc_add_notice(
				sprintf( 'You must have an order with a minimum of %s to place your order, your current order total is %s.',
					wc_price( $minimum ),
					wc_price( WC()->cart->total )
				), 'error'
			);

		}
	}

}

/**
 * Add checkbox field to the checkout
 **/
add_action( 'woocommerce_after_order_notes', 'slx_checkout_field' );

function slx_checkout_field( $checkout ) {

	echo '<div id="my-new-field">';

	woocommerce_form_field( 'let_us_know', array(
		'type'     => 'textarea',
		'class'    => array( 'textarea' ),
		'label'    => __( 'Let us know' ),
		'required' => true,
	), $checkout->get_value( 'let_us_know' ) );

	echo '</div>';

	echo '<div id="my-new-field">';

	woocommerce_form_field( 'substitute', array(
		'type'     => 'checkbox',
		'class'    => array( 'input-checkbox' ),
		'label'    => __( 'Substitute products in my order where items are out of stock' ),
		'required' => true,
	), $checkout->get_value( 'substitute' ) );

	echo '</div>';
}

/**
 * Update the order meta with field value
 **/
add_action( 'woocommerce_checkout_update_order_meta', 'slx_checkout_field_update_order_meta' );

function slx_checkout_field_update_order_meta( $order_id ) {
	if ( $_POST['substitute'] ) {
		update_post_meta( $order_id, 'Substitute', esc_attr( $_POST['substitute'] ) );
	}
}

/* Populate batch with available orders */
function acf_load_batch_field_choices( $field ) {

	global $post;

	/* Output orders as a checkbox list */
	$args = array(
		'posts_per_page' => - 1,
		'post_type'      => 'shop_order',
		'post_status'    => array( 'wc-processing', 'wc-completed' )
	);

	$loop = new WP_Query( $args );

	$batch_id = get_the_ID();

	while ( $loop->have_posts() ) : $loop->the_post();

		if ( ! get_post_meta( $post->ID, 'batch_status', true ) ) {
			$field['choices'][ $post->ID ] = 'Order #' . $post->ID;
		}

		if ( get_post_meta( $post->ID, 'batch_status', true ) == $batch_id ) {
			$field['choices'][ $post->ID ] = 'Order #' . $post->ID;
		}

	endwhile;

	return $field;
}

add_filter( 'acf/load_field/name=orders', 'acf_load_batch_field_choices', 19 );

function save_batch( $post_id ) {

	global $wpdb;

	$orders = get_field( 'orders', $post_id );


	$wpdb->query( "DELETE FROM slx_postmeta WHERE meta_value = '$post_id' AND meta_key = 'batch_status'" );

	foreach ( $orders as $order ) {
		update_post_meta( $order, 'batch_status', $post_id );
	}

}

add_action( 'acf/save_post', 'save_batch', 20 );


add_filter( 'woocommerce_loop_add_to_cart_link', 'quantity_inputs_for_woocommerce_loop_add_to_cart_link', 10, 2 );
function quantity_inputs_for_woocommerce_loop_add_to_cart_link( $html, $product ) {
	if ( $product && $product->is_type( 'simple' ) && $product->is_purchasable() && $product->is_in_stock() && ! $product->is_sold_individually() ) {
		$html = '<form action="' . esc_url( $product->add_to_cart_url() ) . '" class="cart" method="post" enctype="multipart/form-data">';
		$html .= woocommerce_quantity_input( array(), $product, false );
		$html .= '<button type="submit" class="button alt">' . esc_html( $product->add_to_cart_text() ) . '</button>';
		$html .= '</form>';
	}

	return $html;
}

add_action( 'woocommerce_cart_calculate_fees', 'slx_custom_surcharge' );
function slx_custom_surcharge() {
	/* Add Donations To Order Total */
	if ( $_POST['donations'] ) {
		WC()->session->set( 'donations', $_POST['donations'] );
	}

	global $woocommerce;
	foreach ( WC()->session->get( 'donations' ) as $key => $value ) {
		if ( $value > 0 ) {
			$woocommerce->cart->add_fee( __( get_the_title( $key ), 'woocommerce' ), $value );
		}
	}
}

add_filter( 'woocommerce_add_to_cart_fragments', 'slx_header_add_to_cart_fragment' );
function slx_header_add_to_cart_fragment( $fragments ) {
	ob_start();
	?>
	<a class="cart-contents" href="<?php echo wc_get_cart_url(); ?>"
	   title="<?php _e( 'View your shopping cart' ); ?>"><?php echo sprintf( WC()->cart->get_cart_contents_count(), WC()->cart->get_cart_contents_count() ); ?>
		- <?php echo WC()->cart->get_cart_total(); ?></a>
	<?php

	$fragments['a.cart-contents'] = ob_get_clean();

	return $fragments;
}

remove_action( 'woocommerce_before_checkout_form', 'woocommerce_checkout_coupon_form', 10 );
add_action( 'woocommerce_checkout_before_order_review', 'woocommerce_checkout_coupon_form', 10 );

function woocommerce_output_content_wrapper() {
	echo '<div class="container">';
}

function woocommerce_output_content_wrapper_end() {
	echo '</div>';
	echo get_sidebar( 'shop' );
	echo '</div>';
}

// Remove woocommerce breadcrumb
remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20 );

// Remove count and ordering controls
remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30 );
remove_action( 'woocommerce_before_shop_loop', 'woocommerce_result_count', 20 );

add_action( 'woocommerce_before_shop_loop', 'slx_toggle_view', 5 );
function slx_toggle_view() {
	wc_get_template( 'loop/toggle-view.php' );
}


add_action( 'woocommerce_before_shop_loop', 'slx_category_message', 20 );
function slx_category_message() {

	$queried_object = get_queried_object();
	$cat_message    = null;
	$field_id       = 'cat_message';

	if ( $queried_object->post_type == 'page' ) {
		return;
	}

	if ( $queried_object->parent != 0 ) {
		$term        = get_term_by( 'id', $queried_object->parent, 'product_cat' );
		$cat_message = get_field( $field_id, $term );
		if ( ! $cat_message ) {
			return;
		}
		echo '<div class="woocommerce-message">' . $cat_message . '</div>';
	}

	if ( get_field( $field_id, $queried_object ) ) {
		$cat_message = get_field( $field_id, $queried_object );
		if ( ! $cat_message ) {
			return;
		}
		echo '<div class="woocommerce-message">' . $cat_message . '</div>';
	}

}

// Register Sidebar Menu
register_nav_menus(
	array(
		'primary'   => __( 'Primary Menu', 'slx' ),
		'secondary' => __( 'Secondary Menu', 'slx' ),
		// sidebar menu
		'sidebar'   => __( 'Sidebar Menu', 'slx' ),
	)
);

// Thumbnail wrapper start
add_action( 'woocommerce_before_shop_loop_item', 'slx_product_thumbnail_start', 10 );
function slx_product_thumbnail_start() {
	echo '<div class="product-thumbnail" data-image="' . woocommerce_get_product_thumbnail_url() . '">';
}

// Replace store notice
function woocommerce_demo_store() {}

function woocommerce_template_loop_product_thumbnail() {}

function woocommerce_get_product_thumbnail_url( $size = 'shop_catalog' ) {
	global $post;
	$image_size = apply_filters( 'single_product_archive_thumbnail_size', $size );
	if ( has_post_thumbnail() ) {
		return get_the_post_thumbnail_url( $post, $image_size );
	} elseif ( wc_placeholder_img_src() ) {
		// return wc_placeholder_img( $image_size ));
		return wc_placeholder_img_src();
	}

	return false;
}

// Thumbnail wrapper end
add_action( 'woocommerce_before_shop_loop_item_title', 'slx_product_thumbnail_end', 60 );
function slx_product_thumbnail_end() {
	echo '</div>';
}

// Product Details wrapper start
add_action( 'woocommerce_after_shop_loop_item', 'slx_product_details_start', 6 );
function slx_product_details_start() {
	echo '<div class="product-actions">';
}

// change sale flash priority
// make sure the priority is not later than that of `slx_product_details_end`
remove_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_show_product_loop_sale_flash', 10 );
add_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_show_product_loop_sale_flash', 50 );

// add description to product list
add_action( 'woocommerce_after_shop_loop_item', 'slx_show_product_description', 70 );
function slx_show_product_description () {
	global $post;
	echo '<div class="product-description">' . mb_strimwidth($post->post_content, 0, 150, "&hellip;") . '</div>';
}

// Product Details wrapper end
add_action( 'woocommerce_after_shop_loop_item', 'slx_product_details_end', 60 );
function slx_product_details_end() {
	echo '</div>';
}

add_action( 'woocommerce_before_single_product_summary', 'slx_product_summary_start', 5 );
function slx_product_summary_start() {
	echo '<div class="product-summary clearfix">';
}

add_action( 'woocommerce_after_single_product_summary', 'slx_product_summary_end', 11 );
function slx_product_summary_end() {
	echo '</div><!-- after description-->';
}

function woocommerce_output_product_data_tabs() {
	wc_get_template( 'single-product/tabs/tabs.php' );
}

add_action( 'woocommerce_cart_collaterals', 'slx_display_guarantee', 10 );
function slx_display_guarantee() {
	wc_get_template( 'cart/guarantee.php' );
}

// Remove product meta
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );
