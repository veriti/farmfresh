<?php
	/*-----------------------------------------------------------------------------------*/
	/* This template will be called by all other template files to begin
	/* rendering the page and display the header/nav
	/*-----------------------------------------------------------------------------------*/
?>
<!DOCTYPE html>
<!--[if !IE]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<!--[if IE 6 ]><html <?php language_attributes(); ?> class="ie ie6"><![endif]-->
<!--[if IE 7 ]><html <?php language_attributes(); ?> class="ie ie7"><![endif]-->
<!--[if IE 8 ]><html <?php language_attributes(); ?> class="ie ie8"><![endif]-->
<!--[if IE 9 ]><html <?php language_attributes(); ?> class="ie ie9"><![endif]-->
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<title><?php if(is_front_page()): echo get_bloginfo('description') .' - '. get_bloginfo('name'); else: wp_title(); endif; ?></title>

<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<?php // We are loading our theme directory style.css by queuing scripts in our functions.php file,
	// so if you want to load other stylesheets,
	// I would load them with an @import call in your style.css
?>

<?php // Loads HTML5 JavaScript file to add support for HTML5 elements in older IE versions. ?>
<!--[if lt IE 9]>
<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js" type="text/javascript"></script>
<![endif]-->

<?php wp_head(); ?>

<?php if( get_field('analytics', 'option') ){ ?>
	<?php the_field('analytics', 'option'); ?>
<?php } ?>

</head>

<body <?php body_class(); ?>>
<?php // Add Google Tag Manager from "Admin Site Details - Tracking Codes"
	if( get_field('google_tag_manager', 'option') ){
		the_field('google_tag_manager', 'option');
	}
?>
<div id="wrapper" class="<?php $theme_name = get_current_theme(); echo $theme_name; ?> <?php the_field('body_background_type', 'option'); ?>"
<?php if(get_field('body_background_type', 'option') == 'body_background_type_colour'){ ?>
	style="background-color: <?php the_field('body_background_colour', 'option'); ?>"
<?php } ?>
<?php if(get_field('body_background_type', 'option') == 'body_background_type_pattern'){ ?>
	style="background-image: url('<?php the_field('body_background_pattern', 'option'); ?>');"
<?php } ?>
>

<header class="<?php the_field('header_background_type', 'option'); ?>"
<?php if(get_field('header_background_type', 'option') == 'header_background_type_colour'){ ?>
	style="background-color: <?php the_field('header_background_colour', 'option'); ?>"
<?php } ?>
<?php if(get_field('header_background_type', 'option') == 'header_background_type_pattern'){ ?>
	style="background-image: url('<?php the_field('header_background_pattern', 'option'); ?>');"
<?php } ?>
>
<div class="sidenav_icon"><i class="fa fa-bars"></i></div>
<div class="container">
	<div class="row">
		<div class="col-sm-6">
			<div class="logo_holder">
				<?php if (get_field('logo_header', 'option')): ?>
					<?php $image = wp_get_attachment_image_src(get_field('logo_header', 'option'), 'full'); ?>
					<a id="logo" href="/" title=""><img src="<?php echo $image[0]; ?>" alt="<?php bloginfo('name'); ?>" /></a>
				<?php else: ?>
					<span id="head_name"><?php bloginfo('name'); ?></span>
					<span id="head_desc"><?php bloginfo('description'); ?></span>
				<?php endif; ?>
			</div>
		</div>

		<div class="col-sm-6">
			<div class="head_controls">

				<div class="cart-info">
					<?php /* LOGIN / LOGOUT */ ?>
					<span class="cart-info__user"><?php wp_loginout(); ?></span>

					<?php /* CART INFO */ ?>
					<a class="cart-info__status" href="<?php echo wc_get_cart_url(); ?>" title="<?php _e( 'View your shopping cart' ); ?>">(<?php echo sprintf ( WC()->cart->get_cart_contents_count(), WC()->cart->get_cart_contents_count() ); ?>) <i class="fa fa-shopping-basket"></i> <?php echo WC()->cart->get_cart_total(); ?></a>
				</div>

				<?php /* HEADER TEXT */ ?>
				<p><img src="<?php echo get_stylesheet_directory_uri() . '/images/logo-free-delivery.png' ?>" alt="FREE DELIVERY"><br><strong>Your first 4 deliveries are FREIGHT FREE*</strong><br>Brisbane metro only. Orders over $100</p>

			</div>
		</div>
	</div><!-- /row -->
</div><!-- /container -->
</header>

<?php
	get_template_part( 'partials/navigation' );
	get_template_part( 'partials/banner' );
?>
