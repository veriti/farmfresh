<?php
/* Load WordPress Environment */
include("../../../wp-config.php");
   
   /* Query all Customers */
   $customers = get_users();

   $start = mysql_real_escape_string($_GET['start']);
   $finish = mysql_real_escape_string($_GET['finish']);
?>
<html>
   <head>
      <title>Customer Report</title>
      <style>
      body { 
      	font-family: Arial;
       }
      </style>
   </head>
   <body>
   <table border=1 cellpadding=4 cellspacing=0>
      <tr>
         <td bgcolor='aaaaaa'>&nbsp;</td>
         <td bgcolor='aaaaaa'><b>First Name</b></td>
         <td bgcolor='aaaaaa'><b>Last Name</b></td>
         <td bgcolor='aaaaaa'><b>City</b></td>
         <td bgcolor='aaaaaa'><b>Mobile</b></td>
         <td bgcolor='aaaaaa'><b>Email</b></td>
         <td bgcolor='aaaaaa'><b>Address 1</b></td>
         <td bgcolor='aaaaaa'><b>Address 2</b></td>
         <td bgcolor='aaaaaa'><b>Post Code</b></td>
         <td bgcolor='aaaaaa'><b>Total Orders</b></td>
         <td bgcolor='aaaaaa'><b>Order Total</b></td>
         <td bgcolor='aaaaaa'><b>Last Order Date</b></td>
      </tr>
      <?php 
      foreach($customers as $customer): 

        $args = array(
              'numberposts' => -1,
              'meta_key'    => '_customer_user',
              'meta_value'  => $customer->ID,
              'post_type'   => array( 'shop_order' ),
              'post_status' => array( 'wc-processing','wc-completed' ),
              'date_query' => array(
                array(
                  'after'     => $start,
                  'before'     => $finish,
                )
              ),
        );

      	$customer_orders = new WP_Query( $args );

        $total_order = 0;
        $count_order = 0;
        $last_order = '';
 
        while ( $customer_orders->have_posts() ) : $customer_orders->the_post();

           /* Calculate Totals */
           $order = wc_get_order(get_the_ID());

           $total_order += $order->get_total();
           $count_order++;

        endwhile; 

        $args = array(
              'numberposts' => -1,
              'meta_key'    => '_customer_user',
              'meta_value'  => $customer->ID,
              'post_type'   => array( 'shop_order' ),
              'post_status' => array( 'wc-processing','wc-completed' ),
              'orderby' => 'date',
              'order' => 'ASC'
        );

        $customer_orders = new WP_Query( $args );

        while ( $customer_orders->have_posts() ) : $customer_orders->the_post();

           $order = wc_get_order(get_the_ID());
           $last_order = date('d/m/Y', strtotime($order->order_date));
        endwhile;

       

        if($count_order > 0):
       ?>
       <tr>
         <td bgcolor='aaaaaa'><font size='2'>1</font></td>
         <td><font size='2'><?=$customer->billing_first_name;?></font></td>
         <td><font size='2'><?=$customer->billing_last_name;?></font></td>
         <td><font size='2'><?=$customer->billing_city;?></font></td>
         <td><font size='2'><?=$customer->billing_phone;?></font></td>
         <td><font size='2'><?=$customer->billing_email;?></font></td>
         <td><font size='2'><?=$customer->billing_address_1;?></font></td>
         <td><font size='2'><?=$customer->billing_address_2;?></font></td>
         <td><font size='2'><?=$customer->billing_postcode;?></font></td>
         <td><font size='2'><?=$count_order;?></font></td>
         <td><font size='2'>$<?=$total_order;?></font></td>
         <td><font size='2'><?=$last_order;?></font></td>
      </tr>
      <? endif; ?>
      <?php  endforeach;  ?>
      </table>
   </body>
</html>