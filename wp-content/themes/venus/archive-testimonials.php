<?php
get_header();
?>

<div id="content">
	<div class="fruits fruits-left"></div>
	<div class="fruits fruits-right"></div>
	<div class="container">
		<div class="row">
			<div id="mainContent" class="col-sm-10 col-sm-offset-1">

				<h1>Testimonials</h1>
				<hr>

				<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

					<div class="col-sm-4">
						<div>
							<?php the_content(); ?>
							<p>- <strong><?php the_title(); ?></strong> -</p>
						</div>
					</div>

				<?php endwhile; else : ?>

					<article class="post error">
						<h1 class="404">Nothing posted yet</h1>
					</article>

				<?php endif; ?>
			</div>

		</div><!-- row -->
	</div><!-- container -->
</div><!-- content -->

<?php get_footer(); ?>
