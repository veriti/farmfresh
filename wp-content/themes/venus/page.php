<?php
	/* The template for displaying any single page. */
	get_header();
	// get_template_part( 'partials/breadcrumbs' );
?>

<div id="content">

		<div class="fruits fruits-left"></div>
		<div class="fruits fruits-right"></div>
<div class="container">
	<div class="row">
		<div id="mainContent" class="col-sm-10 col-sm-offset-1">
			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>



				<?php if ( get_field('featured_image') && !$post->post_content=="" ) { ?>

					<div class="row">
						<?php if ( get_field('featured_image_type')=='featured_long' ) { ?>

							<div class="col-sm-12">
								<div class="featured_image">
									<?php $image = wp_get_attachment_image_src(get_field('featured_image'), 'cpt_banner_image'); ?>
									<?php $imageF = wp_get_attachment_image_src(get_field('featured_image'), 'full'); ?>
									<a href="<?php echo $imageF[0]; ?>" rel="gallery">
										<img src="<?php echo $image[0]; ?>" />
									</a>
								</div>
							</div>
							<div class="col-sm-12">
								<?php the_content(); ?>
							</div>

						<?php } elseif ( get_field('featured_image_type')=='featured_short' ) { ?>

							<div class="col-lg-4 col-md-6 col-sm-12">
								<div class="featured_image">
									<?php $image = wp_get_attachment_image_src(get_field('featured_image'), 'full'); ?>
									<a href="<?php echo $image[0]; ?>" rel="gallery">
										<img src="<?php echo $image[0]; ?>" />
									</a>
								</div>
							</div>
							<div class="col-lg-8 col-md-6 col-sm-12">
								<?php the_content(); ?>
							</div>

						<?php } ?>
					</div>

				<?php } elseif ( get_field('featured_image') && $post->post_content=="" ) { ?>

					<div class="row">
						<div class="col-sm-12">
							<div class="featured_image">
								<?php $image = wp_get_attachment_image_src(get_field('featured_image'), 'cpt_banner_image'); ?>
								<?php $imageF = wp_get_attachment_image_src(get_field('featured_image'), 'full'); ?>
								<a href="<?php echo $imageF[0]; ?>" rel="gallery">
									<img src="<?php echo $image[0]; ?>" />
								</a>
							</div>
						</div>
					</div>

				<?php } elseif ( !$post->post_content=="" ) { ?>

					<div class="row">
						<div class="col-sm-12">
							<?php the_content(); ?>
						</div>
					</div>

				<?php } ?>

			<?php endwhile; else : ?>

				<article class="post error">
					<h1 class="404">No Pages have been posted yet</h1>
				</article>

			<?php endif; ?>
		<?

		if(is_page('postcode-search')):

		$postcode = esc_sql($_GET['postcode']);
		global $wpdb;

		$args = array(
		    'post_type' => 'deliveryexception'
		);


		$postcodes = new WP_query($args);

		if($postcodes->have_posts()) :
	     	while($postcodes->have_posts()) :  $postcodes->the_post();

	     	if($postcode >= get_field('postcode_start') && $postcode <= get_field('postcode_end')) {
				$locations[] = get_the_title();
			}
	     	endwhile;
     	endif;

     	$locations = array_unique($locations);

		?>
		<form action="">
		<p>Postcode: <input type="text" name="postcode" value=""> <input type="submit" value="Search"></p>

		<table width="100%">
		<?php
			/* Query the Suburb name from Postcode */
			$geocode = $wpdb->get_results("SELECT * FROM postcodes_geo WHERE postcode = '$postcode'");

			foreach($geocode as $geo):
		?>
			<tr>
				<td valign="top"><?php echo $_GET['postcode'];?></td>
				<td valign="top"><?php echo $geo->suburb; ?></td>
				<td><strong>Tuesday - Afternoon Delivery</strong><br/>*If ordered by Monday 8.30AM <br/><hr/><strong>Tuesday - Afternoon Delivery</strong><br/>*If ordered by Monday 8.30AM<br/><br/></td>
				<td valign="top">FREE*</td>
			</tr>
			<? endforeach; ?>
		</table>
		<?
			if(!$geocode):
				echo "<p>Sorry we do not deliver to that postcode. Please check back later.</p>";
			endif;
		?>
		</form>
		<?php endif; ?>

		</div>

	</div><!-- row -->
</div><!-- container -->
</div><!-- content -->

<?php
	if ( wp_get_theme() != 'Venus' ) {
		if( get_field('services_box', 'option') ){
			get_template_part( 'partials/box_links_holder' );
		}
	}
?>

<?php get_footer(); ?>
