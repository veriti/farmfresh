<?php
/* Load WordPress Environment */
include("../../../wp-config.php");

$batchID = mysql_real_escape_string($_GET['batchID']);

/* Get the orders in the batch */
$orders = get_field('orders',$batchID);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<!-- saved from url=(0067)http://admin.freshorganics.com.au/reports/invoice_multi.asp?id=2421 -->
<html><head><meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>Batch invoices for batch #<?php echo $batchID; ?></title>

<style type="text/css">
<!--
.blocks {
  border: 1px solid #000000;
}
body {
  font-family: Verdana, Arial, Helvetica, sans-serif;
  font-size: 11px;
}
.style1 {
  font-size: 14px;
  font-weight: bold;
}
.style2 {
  font-size: 13px;
  font-weight: bold;
}
.style4 {font-size: 16px; font-weight: bold; }
.style5 {
  font-size: 12px;
  font-weight: bold;
}
-->
</style>
<body>

<?php

/* Output Orders Loop */
foreach($orders as $order):

/* Order Details */
$details = new WC_Order($order);

?>
<table style="page-break-before:always"><tbody><tr><td>
<table border="0" cellpadding="1" cellspacing="1" style="width:160mm;">
  <tbody><tr>
    <td height="84" colspan="2" valign="top"><!--<img src="http://www.freshorganics.com.au/report-batch_files/logo.gif" width="309" height="57">--><h1>Farm Fresh Organics</h1></td>
    <td width="333" valign="top"><div align="right"><span class="style1">Farm Fresh Organics</span><br>
      ABN : 31 413 831 583<br>
      <strong>P: 1300 72 66 71<br></strong><strong>F: 07 3162 3928</strong><br>
5/9 Shoebury St<br>
Rocklea 4106<br>
www.freshorganics.com.au</div></td>
  </tr>
  <tr>
    <td width="37" height="89" valign="top">To: </td>
    <td width="263" valign="top"><span class="style2"><em><?php echo $details->get_billing_address(); ?></span></td>
    <td><div align="right"><br>
        <span class="style4">TAX INVOICE</span><br>
      <br>
      <table width="88%" border="0" cellspacing="0" cellpadding="2">
        <tbody><tr>
          <td width="26%" class="blocks">Date</td>
          <td width="36%" class="blocks">Invoice No. </td>
          </tr>
        <tr>
          <td class="blocks"><?php echo date("d-m-Y"); ?></td>
          <td class="blocks"><?php echo $order; ?></td>
          </tr>
      </tbody></table> 
    </div></td>
  </tr>
  <tr>
    <td colspan="3" valign="top"><p><br>
      Delivery Instructions:<br/> 
      <?=$details->customer_message;?>
      <br>
</p>
      <table border="0" cellpadding="0" cellspacing="0" style="width:158mm;">
        <tbody><tr>
          <td style="width:10mm;"><strong>Qty</strong></td>
          <td style="width:80mm;"><strong>Product</strong></td>
          <td style="width:20mm;" align="right"><strong>Price/Unit</strong></td>
          <td style="width:20mm;" align="right"><strong>Total</strong></td>
          <td style="width:28mm;" align="right"><strong>Included GST</strong></td>
        </tr>
        <?
        /* Output products and pricing in the order */
        $product = $details->get_items(); 
        foreach($product as $order_product_detail):
        ?>
        <tr>
          <td style="width:10mm;" valign="top"><? echo $order_product_detail['qty']; ?></td>
          <td style="width:80mm;"><? echo $order_product_detail['name']; ?></td>
          <td style="width:20mm;" align="right" valign="top"><? echo number_format($order_product_detail['line_subtotal'] / $order_product_detail['qty'],2); ?></td>
          <td style="width:20mm;" align="right" valign="top"><? echo number_format($order_product_detail['line_subtotal'],2); ?></td>
          <td style="width:28;" align="right" valign="top"><? echo number_format($order_product_detail['line_subtotal_tax'],2); ?></td>
        </tr>
        <tr>
          <td colspan="5"><br></td>
        </tr>
       <? endforeach; ?>
        <tr>
          <td height="21" colspan="3" align="right" style="width:130mm;height=10mm;"><b>Subtotal</b></td>
          <td style="width:20mm;height=10mm;" align="right"><div align="right"><b> <?=$details->get_total();?></b></div></td>
          <td style="width:28mm;height=10mm;" align="right"><b><?=$details->get_total_tax();?></b></td>
        </tr>
        <tr>
          <td height="21" colspan="3" align="right" style="width:130mm;height=10mm;"><strong>Adjustments</strong></td>
          <td style="width:20mm;height=10mm;" align="right">&nbsp;</td>
          <td style="width:28mm;height=10mm;" align="right">&nbsp;</td>
        </tr>
        <tr>
          <td height="21" colspan="3" align="right" style="width:130mm;height=10mm;"><span class="style5">Total</span></td>
          <td style="width:20mm;height=10mm;" align="right">&nbsp;</td>
          <td style="width:28mm;height=10mm;" align="right">&nbsp;</td>
        </tr>

      </tbody></table>
      <table border="0" cellpadding="1" cellspacing="0">
        <tbody><tr>
          <td align="right">Substitute:&nbsp;&nbsp;</td>
          <td>Yes</td>
        </tr>
        <tr>
          <td align="right">Special Ordering Requests:&nbsp;&nbsp;</td>
          <td><?=$details->customer_message;?></td>
        </tr>
        <tr>
          <td align="right">Payment by:&nbsp;&nbsp;</td>
          <td>Direct Deposit</td>
        </tr>
      </tbody></table>      <p align="center">Thanks very much for shopping with us!  We appreciate your support of our family business.<br><br>FRESH &amp; GREEN DOLLARS are a loyalty reward issued per $100 per order, and are redeemable in bundles of 10 only. Discount is applied only AFTER vouchers are posted with your name and address to Farm Fresh Organics. Maximum discount $10 per order.<br><br>DD payment details: BSB 484-799  AC: 201799606 Surname as reference. PLEASE NOTE payment by direct deposit involves responsibility for prompt payment - i.e. within two days of delivery date. If you know you're paying late please notify us OR add a $5 admin fee to your late payment.</p></td>
  </tr>
</tbody></table>
</td></tr></tbody></table>
<?php
/* End Orders Loop */
endforeach; 
?>
</body>
</html>