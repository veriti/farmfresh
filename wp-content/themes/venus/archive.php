<?php
	/* The template for displaying Archive pages */
	get_header();
	$post_type = get_post_type();
	if ( $post_type )
	{
		$post_type_data = get_post_type_object( $post_type );
		$post_type_slug = $post_type_data->rewrite['slug'];
		$object = get_post_type_object( $post_type_slug );
	}
?>

<div id="content">
<div class="container">
	<div class="row">
		<div id="mainContent" class="col-sm-12">

			<div class="row">
				<div class="col-sm-12">
					<h1><?php echo $object->label; ?></h1>
				</div>
			</div>

			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

				<div class="custom_post_item<?php if ( is_post_type_archive( 'gallery' ) ) { ?> <?php the_field('gallery_type', 'option'); ?><?php } ?>">

					<?php if ( is_post_type_archive( 'testimonial' )){ ?>

						<div class="row">
							<div class="col-sm-12">
								<?php the_content(); ?>
								<p class="fw_400">- <?php the_title(); ?></p>
							</div>
						</div>

					<?php } elseif ( is_post_type_archive( 'gallery' )){ ?>

						<?php if (get_field('gallery_type', 'option') == 'gallery_type_portfolio'){ ?>

							<div class="row">
								<div class="col-sm-12">
									<h4 class="title"><?php the_title(); ?></h4>
									<?php the_content(); ?>
								</div>
							</div>

							<div class="row">
								<div class="col-sm-12">
									<div class="gallery_holder">
										<?php $image = wp_get_attachment_image_src(get_field('featured_image'), 'full'); ?>
										<a href="<?php echo $image[0]; ?>">
											<img src="<?php echo $image[0]; ?>" />
										</a>
										<?php
											$images = get_field('gallery');
											if( $images ):
											foreach( $images as $image ):
										?>
											<a href="<?php echo $image['url']; ?>" rel="gallery">
												<img src="<?php echo $image['sizes']['ratio3-2']; ?>" alt="<?php echo $image['alt']; ?>" />
											</a>
										<?php endforeach; endif; ?>
									</div><!-- galleryHolder -->
								</div>
							</div>

						<?php } else if (get_field('gallery_type', 'option') == 'gallery_type_list'){ ?>

							<div class="row">
								<?php if (get_field('featured_image')): ?>
									<div class="col-lg-3 col-md-5 col-sm-12">
										<div class="featured_image">
											<?php $image = wp_get_attachment_image_src(get_field('featured_image'), 'full'); ?>
											<a href="<?php echo get_permalink(); ?>">
												<img src="<?php echo $image[0]; ?>" />
											</a>
										</div>
									</div>
									<div class="col-lg-9 col-md-7 col-sm-12">
										<h4 class="title slx_mb_0"><a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a></h4>
										<?php the_field('short_description'); ?>
										<p class="slx_mt_-20"><a class="tt_cap" href="<?php echo get_permalink(); ?>">View Full <?php echo $post_type_slug; ?></a></p>
									</div>
								<?php else: ?>
									<div class="col-sm-12">
										<h4 class="title slx_mb_0"><a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a></h4>
										<?php the_field('short_description'); ?>
										<p class="slx_mt_-20"><a class="tt_cap" href="<?php echo get_permalink(); ?>">View Full <?php echo $post_type_slug; ?></a></p>
									</div>
								<?php endif; ?>
							</div>

						<?php } ?>

					<?php } else { ?>

						<div class="row">
							<?php if (get_field('featured_image')): ?>
								<div class="col-lg-3 col-md-5 col-sm-12">
									<div class="featured_image">
										<?php $image = wp_get_attachment_image_src(get_field('featured_image'), 'full'); ?>
										<a href="<?php echo get_permalink(); ?>">
											<img src="<?php echo $image[0]; ?>" />
										</a>
									</div>
								</div>
								<div class="col-lg-9 col-md-7 col-sm-12">
									<h4 class="title slx_mb_0"><a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a></h4>
									<?php the_field('short_description'); ?>
									<p class="slx_mt_-20"><a class="tt_cap" href="<?php echo get_permalink(); ?>">View Full <?php echo $post_type_slug; ?><?php if ( is_post_type_archive( 'staff' ) ) { ?> Profile<?php } ?></a></p>
								</div>
							<?php else: ?>
								<div class="col-sm-12">
									<h4 class="title slx_mb_0"><a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a></h4>
									<?php the_field('short_description'); ?>
									<p class="slx_mt_-20"><a class="tt_cap" href="<?php echo get_permalink(); ?>">View Full <?php echo $post_type_slug; ?><?php if ( is_post_type_archive( 'staff' ) ) { ?> Profile<?php } ?></a></p>
								</div>
							<?php endif; ?>
						</div>

					<?php } ?>

				</div><!-- custom_post_item -->

			<?php endwhile; else : ?>

				<article class="post error">
					<h1 class="404">Nothing posted yet</h1>
				</article>

			<?php endif; ?>
		</div>

	</div><!-- row -->
</div><!-- container -->
</div><!-- content -->

<?php get_footer(); ?>
